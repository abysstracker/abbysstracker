<?php
/**
 * Created by PhpStorm.
 * User: Lobash
 * Date: 12.04.2018
 * Time: 23:34
 */

namespace app\Components;


class DomParser
{

    /**
     * Возвращает объект DOM парсера
     * @return simple_html_dom
     */
    public static function getParserObject() {
        require_once '../libs/simplehtmldom_1_5/simple_html_dom.php';
        $oParser = new \simple_html_dom();
        return $oParser;
    }

}