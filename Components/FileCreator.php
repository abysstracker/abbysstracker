<?php


namespace app\Components;


class FileCreator {

    const FILE_ENCODING = 'UTF-8';

    public static function createFile($aData){
        $sFileName = self::generateName();

        $oXlsWriter = new \Spreadsheet_Excel_Writer();
        $oXlsWriter->setVersion(8);

        $oHeadRowFormat = self::parseHeadFormat($oXlsWriter);
        $oRowFormat = self::parseRowFormat($oXlsWriter);

        $oSheet = $oXlsWriter->addWorksheet('Tags');
        self::writeHead($oSheet, $oHeadRowFormat);
        self::writeBody($aData, $oSheet, $oRowFormat);

        $oXlsWriter->send($sFileName);
        $oXlsWriter->close();
    }

    /**
     * @return array
     */
    private static function getXlsHeaders()
    {
        $header = array("parent", "Шаблон", "Тип видимости", "Название", "alt_title", "Title", "Description", "Keywords", "staticContent", "staticContent2", "alias");
        return $header;
    }

    /**
     * @return string
     */
    private static function generateName()
    {
        $sFileName = 'export_structure_' . date("Y-m-d_H:i:s") . '.xls';
        return $sFileName;
    }

    /**
     * @param \Spreadsheet_Excel_Writer $oXlsWriter
     * @return mixed
     */
    private static function parseHeadFormat($oXlsWriter)
    {
        $colHeadingFormat = $oXlsWriter->addFormat();
        $colHeadingFormat->setAlign("center");
        $colHeadingFormat->setBold();
        return $colHeadingFormat;
    }

    /**
     * @param \Spreadsheet_Excel_Writer $oXlsWriter
     * @return mixed
     */
    private static function parseRowFormat($oXlsWriter)
    {
        $colFormat = $oXlsWriter->addFormat();
        $colFormat->setAlign('justify');
        return $colFormat;
    }

    /**
     * @param \Spreadsheet_Excel_Writer_Worksheet $oSheet
     * @param int $oHeadRowFormat
     */
    private static function writeHead($oSheet, $oHeadRowFormat)
    {
        $oSheet->setInputEncoding(self::FILE_ENCODING);

        $oSheet->setColumn(0, 10, 50);
        $oSheet->setRow(0, 40);

        $aHeaders = self::getXlsHeaders();
        $oSheet->writeRow(0, 0, $aHeaders, $oHeadRowFormat);
    }

    /**
     * @todo сделать валидацию по header
     * @param $aData
     * @param \Spreadsheet_Excel_Writer_Worksheet $oSheet $oSheet
     * @param $oRowFormat
     */
    private static function writeBody($aData, $oSheet, $oRowFormat)
    {
        $aParent = [];
        $currentRow = 1;
        foreach ($aData as $value) {

            $row = [];
            $row['parent'] = $value['parent'];
            $row['template'] = '7';
            $row['visible'] = 'Видимый';
            $row['title'] = $value['h1'];
            $row['h1'] = $value['h1'];
            $row['seo_title'] = $value['seo_title'];
            $row['seo_description'] = $value['seo_description'];
            $row['seo_keywords'] = $value['seo_keywords'];
            $row['staticContent'] = '';
            $row['staticContent2'] = '';
            $row['alias'] = $value['alias'];

            $oSheet->writeRow($currentRow, 0, $row, $oRowFormat);
            $currentRow++;

            $aParent[$value['id']] = $currentRow;
        }
    }

}