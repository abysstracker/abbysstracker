function empty(sParam) {
    return typeof sParam === 'undefined' || !sParam;
}

function getClickedKey(event) {
    let key = event.currentTarget.dataset.unikKey;
    if (empty(key)) {
        alert("Ошибка получения уникального ключа");
        return false;
    }
    return key;
}

/**
 * получить список алиасов вида /alias/ из
 * урла кликнутого чекбокса
 * @param url
 * @returns {string[]|string[]}
 */
function getCurrentAliasList(url) {
    aAliasList = ['/']; //первая в любом случае "главная" (/)
    if (!empty(url)) {
        let aList = url.split('/').filter(String)
        if (!empty(aList)) {
            for (key in aList) {
                aAliasList.push("/" + aList[key] + "/")
            }
        }
    }

    return aAliasList;
}

function getAllListOnPage() {
    let list = [];
    $.each($('.js_check'), function () {
        list.push($(this).val());
    });

    return list;
}

$(function () {

    $(document).on('click', '.select-on-check-all', function (e) {
        let bCmd = $(this)[0].checked,
            list = getAllListOnPage();

        setExportSession(list, bCmd);
    });

    $(document).on('change', '.js_check', function (e) {
        let val = $(this).val(),
            bCmd = $(this)[0].checked;
        setExportSession([val], bCmd);
    });

    $(document).on('click', '.js_low_level', function (e) {
        let clickedUr = $(this).data('url');
        let keyCheckbox = $(this).data('unik-key');

        if (empty(clickedUr) || empty(keyCheckbox)) {
            alert("Внимание! Ошибка определения выбранной ссылки, обратитесь к разработчику.");
            return false;
        }

        toggleLowLevel(clickedUr, keyCheckbox);
    });

    $(document).on('click', '.js_cur_branch', function (e) {
        let clickedUr = $(this).data('url');
        let keyCheckbox = $(this).data('unik-key');

        if (empty(clickedUr) || empty(keyCheckbox)) {
            alert("Внимание! Ошибка определения выбранной ссылки, обратитесь к разработчику.");
            return false;
        }
        toggleCurBranch(clickedUr, keyCheckbox);
    });

    $(document).on('click', '.js_cur_level', function (e) {
        let clickedLevel = $(this).data('level');
        let keyCheckbox = $(this).data('unik-key');

        if (empty(clickedLevel) || empty(keyCheckbox)) {
            alert("Внимание! Ошибка определения выбранного уровня, обратитесь к разработчику.");
            return false;
        }

        toggleCurLevel(clickedLevel, keyCheckbox);

    });
})

function toggleLowLevel(url, key) {
    let wasChecked = false;
    let $currentCheckbox = $('.js_check[data-unik-key="' + key + '"]');

    if ($currentCheckbox.prop('checked')) {
        wasChecked = true;
    }

    let list = [];
    $.each($('.js_check'), function () {
        if ($(this).data('url') && $(this).data('url').indexOf(url) === 0) {
            $(this).prop('checked', !wasChecked);
            list.push($(this).val());
        }
        setExportSession(list, !wasChecked);
    });

}

/**
 * определяем предыдущее состояние кликнутого чекбокса.
 * до изменения
 * @param value
 * @returns {boolean}
 */
function identifyCheckboxState(value) {
    let wasChecked = false;
    let $currentCheckbox = $('.js_check[data-unik-key="' + value + '"]');

    if ($currentCheckbox.prop('checked')) {
        return true;
    }
    return false;
}

/**
 * Переключение чекбоксов всей ветки
 * @param url
 * @param value
 */
function toggleCurBranch(url, value) {

    //если был чекнут, то отключить чекбоксы, иначе включить
    let wasChecked = identifyCheckboxState(value);
    let aAliasBranch = getCurrentAliasList(url); //убираем пустые элементы

    $.each(aAliasBranch, function () {
        let checkedAlias = this,
            list = [];

        $.each($('.js_check'), function () {
            if ($(this).data('url').indexOf(checkedAlias) === 0) {
                $(this).prop('checked', !wasChecked);
                list.push($(this).val());
            }
            setExportSession(list, !wasChecked);
        })

    })

}

function setExportSession(listVal, bCmd) {
    let cmd = 'add';
    if (!bCmd) {
        cmd = 'remove';
    }

    $.ajax({
        type: "POST",
        url: "set-structure-export",
        data: {
            'cmd': cmd,
            list: listVal
        },

    });
}

function toggleCurLevel(curLevel, key) {

    //если был чекнут, то отключить чекбоксы, иначе включить
    let wasChecked = false,
        $currentCheckbox = $('.js_check[data-unik-key="' + key + '"]'),
        list = [];

    if ($currentCheckbox.prop('checked')) {
        wasChecked = true;
    }

    $.each($('.js_check[data-level="' + curLevel + '"]'), function () {
        $(this).prop('checked', !wasChecked);
        list.push($(this).val());
    });

    setExportSession(list, !wasChecked);
}