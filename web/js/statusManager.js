$(function () {
    $(document).on('click', '.js-repeat', function (e) {
        var id_site = $(this).data('id');
        $("#js-dialog-repeat").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Уверен": function () {
                    $(this).dialog("close");
                    sendCommand(id_site, '/task/repeat/');
                },
                "Нет!": function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    $(document).on('click', '.js-play', function (e) {
        var id_site = $(this).data('id');
        sendCommand(id_site, '/task/start/');
    });

    $(document).on('click', '.js-stop', function (e) {
        var id_site = $(this).data('id');
        sendCommand(id_site, '/task/stop/');
    });
});


function sendCommand(id_site, url_query) {
    // $.ajax('/task/repeat', )
    $.ajax({
        url:url_query,
        data: {
            id:id_site
        },
        method:'GET',
        success:function () {
            location.reload();
        },
        error:function () {
            alert("Произошла ошибка");
        }
    })
}
