<?php
/**
 * Created by PhpStorm.
 * User: jojo
 * Date: 05.08.2018
 * Time: 15:16
 */

class A {

    const PATTERN_CLEAN_DOMAIN = '/^.*?:\/\/.*?(?=\/|$)/';
    const PATTERN_CLEAN_DOMAIN_NEW = '/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/';

    public static function c($object, $method, $args = [])
    {
        $classReflection = new \ReflectionClass(get_class($object));
        $methodReflection = $classReflection->getMethod($method);
        $methodReflection->setAccessible(true);
        $result = $methodReflection->invokeArgs($object, $args);
        $methodReflection->setAccessible(false);
        return $result;
    }

    private function parseAlias($sHref, $sCurrentUrl) {

        if (mb_substr($sHref, 0,2) == '//' OR mb_substr($sHref, 0, 4) == 'http') {
            if (mb_substr($sHref, 0,2) == '//')
                $sHref = mb_substr($sHref, 2);
            else {
                $sHref = str_replace('https://', '', $sHref);
                $sHref = str_replace('http://', '', $sHref);
            }

            //Найдем следующий первый слеш и отрежем как хвост, а то регулярка и хвост накрывает
            $slashPos = mb_strpos($sHref, '/', 3, 'UTF-8');
            if ($slashPos) {
                $sTail = mb_substr($sHref, $slashPos);
                $sHref = mb_substr($sHref, 0, $slashPos);
            } else {
                $sTail = '';
            }

            //Экспереминтальная функция на проверку русских доменов и вообще пуникод
            if ($convert = idn_to_ascii($sHref))
                if ($convert2 = idn_to_utf8($convert) != $convert)
                    $sHref = '';

            // '//site.ru/avatar/98/763762.jpg'
            // возвращает /avatar/98/763762.jpg'
            $sHref = preg_replace(self::PATTERN_CLEAN_DOMAIN_NEW, '', $sHref);
            $sHref .= $sTail;
            $sHref = $this->setFirstSlash($sHref);
        }

        //удалим двойные слэши. тройные нахер, может попозже
        $sHref = str_replace('//', '/', $sHref);


        //если нет первого слеша, то возможно неявно домен прописан, проверим еще раз
        if (mb_substr($sHref, 0,1) != '/') {
            $sHrefCompare = $sHref;
            //Найдем следующий первый слеш и отрежем как хвост, а то регулярка и хвост накрывает
            $slashPos = mb_strpos($sHref, '/', 1, 'UTF-8');
            if ($slashPos) {
                $sTail = mb_substr($sHref, $slashPos);
                $sHref = mb_substr($sHref, 0, $slashPos);
            } else {
                $sTail = '';
            }
            //Экспереминтальная функция на проверку русских доменов и вообще пуникод
            if ($convert = idn_to_ascii($sHref))
                if ($convert2 = idn_to_utf8($convert) != $convert)
                    $sHref = '';


            $sHref = preg_replace(self::PATTERN_CLEAN_DOMAIN_NEW, '', $sHref);

            $sHref .= $sTail;
            //Ничего не вырезали, значит домена нет и это просто корявая относительная ссылка
            if ($sHrefCompare == $sHref) {
                $sHref = $sCurrentUrl .  $sHref;
            }
        }
        $sHref = $this->setLastSlash($sHref);
        $sHref = $this->setFirstSlash($sHref);
        return $sHref;

    }

    /**
     * @param $sHref
     * @return string
     */
    private function setLastSlash($sHref)
    {
        if (mb_substr($sHref, -1) != '/') {
            $sHref .= '/';
        }
        return $sHref;
    }

    /**
     * @param $sHref
     * @return string
     */
    private function setFirstSlash($sHref)
    {
        if (mb_substr($sHref, 0, 1) != '/') {
            return '/' . $sHref;
        }
        return $sHref;
    }



}

function is($a, $b) {
    return $a == $b ? 'YES' : 'NO';
}

?>

<html>
    <body>
        <table>
            <tr>
                <th>parent</th>
                <th>/</th>
            </tr>
            <tr>
                <th>in</th>
                <th>out</th>
                <th>res</th>
            </tr>
            <tr>
                <td>/</td>
                <td>/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/', '/']), '/') ?></td>

            </tr>
            <tr>
                <td>/test/</td>
                <td>/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/test/', '/']), '/test/') ?></td>
            </tr>
            <tr>
                <td>/test</td>
                <td>/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/test', '/']), '/test/') ?></td>
            </tr>
            <tr>
                <td>http://oneclick.ru/</td>
                <td>/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['http://oneclick.ru/', '/']), '/') ?></td>
            </tr>
            <tr>
                <th>parent</th>
                <th>/parent1/</th>
            </tr>
            <tr>
                <th>in</th>
                <th>out</th>
                <th>res</th>
            </tr>
            <tr>
                <td>test</td>
                <td>/parent1/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['test', '/parent1/']), '/parent1/test/') ?></td>
            </tr>
            <tr>
                <td>test/</td>
                <td>/parent1/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['test/', '/parent1/']), '/parent1/test/') ?></td>
            </tr>
            <tr>
                <td>/test</td>
                <td>/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/test', '/parent1/']), '/test/') ?></td>
            </tr>
            <tr>
                <td>/test/</td>
                <td>/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/test/', '/parent1/']), '/test/') ?></td>
            </tr>
            <tr>
                <td>/parent1/test/</td>
                <td>/parent1/test/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/parent1/test/', '/parent1/']), '/parent1/test/') ?></td>
            </tr>
            <tr>
                <td>https://oneclick.ru</td>
                <td>/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru', '/parent1/']), '/') ?></td>
            </tr>
            <tr>
                <td>https://oneclick.ru/parent1/section2</td>
                <td>/parent1/section2/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru/parent1/section2', '/parent1/']), '/parent1/section2/') ?></td>
            </tr>
            <tr>
                <td>https://oneclick.ru/parent1/section2/</td>
                <td>/parent1/section2/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru/parent1/section2/', '/parent1/']), '/parent1/section2/') ?></td>
            </tr>
            <tr>
                <th>parent</th>
                <th>/parent1/parent2/</th>
            </tr>
            <tr>
                <th>in</th>
                <th>out</th>
                <th>res</th>
            </tr>
            <tr>
                <td>https://oneclick.ru/parent1/parent2/</td>
                <td>/parent1/parent2/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru/parent1/parent2/', '/parent1/parent2/']), '/parent1/parent2/') ?></td>
            </tr>
            <tr>
                <td>https://oneclick.ru/parent1/parent2/section3</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru/parent1/parent2/section3', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>https://oneclick.ru/parent1/parent2/section3/</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru/parent1/parent2/section3/', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>https://oneclick.ru//parent1/parent2/section3/</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://oneclick.ru//parent1/parent2/section3/', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>parent1/parent2/section3/</td>
                <td>/parent1/parent2/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['parent1/parent2/section3/', '/parent1/parent2/']), '/parent1/parent2/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>/parent1/parent2/section3/</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['/parent1/parent2/section3/', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>//parent1//parent2//section3//</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['//parent1//parent2//section3//', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>//parent1//parent2/section3//</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['//parent1//parent2/section3//', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>//oneclick.ru/parent1//parent2//section3</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['//oneclick.ru/parent1//parent2//section3', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>//президент.рф/отец//двоих//дочерей</td>
                <td>/отец/двоих/дочерей/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['//президент.рф/отец//двоих//дочерей', '/parent1/parent2/']), '/отец/двоих/дочерей/') ?></td>
            </tr>
            <tr>
                <td>xn--j1ail.xn--p1ai/dveri/</td>
                <td>/dveri/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['xn--j1ail.xn--p1ai/dveri/', '/parent1/parent2/']), '/dveri/') ?></td>
            </tr>
            <tr>
                <td>oneclick.ru/parent1//parent2//section3</td>
                <td>/parent1/parent2/section3/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['oneclick.ru/parent1//parent2//section3', '/parent1/parent2/']), '/parent1/parent2/section3/') ?></td>
            </tr>
            <tr>
                <td>https://datrans.ru/gruzoperevozki-po-rossii</td>
                <td>/gruzoperevozki-po-rossii/</td>
                <td><?= is(A::c(new A, 'parseAlias', ['https://datrans.ru/gruzoperevozki-po-rossii', '/parent1/parent2/']), '/gruzoperevozki-po-rossii/') ?></td>
            </tr>
        </table>

    </body>

</html>
