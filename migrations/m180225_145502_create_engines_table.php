<?php

use yii\db\Migration;

/**
 * Handles the creation of table `engines`.
 */
class m180225_145502_create_engines_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('engines', [
            'id' => $this->primaryKey(),
            'engine' => $this->string(255),
            'title' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('engines');
    }
}
