<?php

use yii\db\Migration;

/**
 * Handles the creation of table `parsed_sites`.
 */
class m180225_150849_create_parsed_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('parsed_sites', [
            'id' => $this->primaryKey(),
            'structure_id' => $this->integer(),
            'domain' => $this->string(256),
            'parent_site' => $this->string(256),
            'date_start' => $this->dateTime(),
            'last_modified' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('parsed_sites');
    }
}
