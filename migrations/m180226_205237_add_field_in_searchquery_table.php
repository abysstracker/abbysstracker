<?php

use yii\db\Migration;

/**
 * Class m180226_205237_add_field_in_searchquery_table
 */
class m180226_205237_add_field_in_searchquery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\SearchQuery::tableName(), 'date_start', $this->dateTime());
        $this->addColumn(\app\models\SearchQuery::tableName(), 'date_end', $this->dateTime());
        $this->addColumn(\app\models\SearchQuery::tableName(), 'column_string_parsed', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(\app\models\SearchQuery::tableName(), 'date_start');
        $this->dropColumn(\app\models\SearchQuery::tableName(), 'date_end');
        $this->dropColumn(\app\models\SearchQuery::tableName(), 'column_string_parsed');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180226_205237_add_field_in_searchquery_table cannot be reverted.\n";

        return false;
    }
    */
}
