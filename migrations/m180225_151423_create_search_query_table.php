<?php

use yii\db\Migration;

/**
 * Handles the creation of table `search_query`.
 */
class m180225_151423_create_search_query_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('search_query', [
            'id' => $this->primaryKey(),
            'engine_id' => $this->integer(),
            'position_limit' => $this->integer(),
            'status' => $this->integer(),
            'search_text' => $this->string(256),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('search_query');
    }
}
