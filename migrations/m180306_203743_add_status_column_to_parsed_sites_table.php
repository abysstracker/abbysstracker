<?php

use yii\db\Migration;

/**
 * Handles adding status to table `parsed_sites`.
 */
class m180306_203743_add_status_column_to_parsed_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('parsed_sites', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('parsed_sites', 'status');
    }
}
