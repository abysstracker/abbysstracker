<?php

use yii\db\Migration;

/**
 * Handles adding search_words to table `search_query`.
 */
class m180510_183233_add_search_words_column_to_search_query_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('search_query', 'search_words', $this->text()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('search_query', 'search_words');
    }
}
