<?php

use yii\db\Migration;

/**
 * Class m180416_193521_insert_yandex
 */
class m180416_193521_insert_yandex extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(\app\models\Engines::tableName(), ['engine', 'title'],[['yandex', 'Яндекс']]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(\app\models\Engines::tableName(), ['engine' => 'yandex']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_193521_insert_yandex cannot be reverted.\n";

        return false;
    }
    */
}
