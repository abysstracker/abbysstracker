<?php

use yii\db\Migration;

/**
 * Class m180402_195252_add_col_region_id_in_search_table
 */
class m180402_195252_add_col_region_id_in_search_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('search_query', 'region_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('search_query', 'region_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_195252_add_col_region_id_in_search_table cannot be reverted.\n";

        return false;
    }
    */
}
