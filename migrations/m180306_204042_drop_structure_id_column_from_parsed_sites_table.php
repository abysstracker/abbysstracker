<?php

use yii\db\Migration;

/**
 * Handles dropping structure_id from table `parsed_sites`.
 */
class m180306_204042_drop_structure_id_column_from_parsed_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('parsed_sites', 'structure_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('parsed_sites', 'structure_id', $this->integer());
    }
}
