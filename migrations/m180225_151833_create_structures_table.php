<?php

use yii\db\Migration;

/**
 * Handles the creation of table `structures`.
 */
class m180225_151833_create_structures_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('structures', [
            'id' => $this->primaryKey(),
            'parsed_site_id' => $this->integer(),
            'is_goods' => $this->integer(),
            'status' => $this->integer(),
            'alias' => $this->string(256),
            'url' => $this->string(256),
            'h1' => $this->string(256),
            'seo_title' => $this->string(256),
            'seo_description' => $this->string(256),
            'seo_keywords' => $this->string(256),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('structures');
    }
}
