<?php

use yii\db\Migration;

/**
 * Class m180416_193602_remove_field_task
 */
class m180416_193602_remove_field_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(\app\models\SearchQuery::tableName(), 'position_limit');
        $this->dropColumn(\app\models\SearchQuery::tableName(), 'date_end');
        $this->dropColumn(\app\models\SearchQuery::tableName(), 'column_string_parsed');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(\app\models\SearchQuery::tableName(), 'position_limit', $this->integer());
        $this->addColumn(\app\models\SearchQuery::tableName(), 'date_end', $this->dateTime());
        $this->addColumn(\app\models\SearchQuery::tableName(), 'column_string_parsed', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_193602_remove_field_task_pos_limit cannot be reverted.\n";

        return false;
    }
    */
}
