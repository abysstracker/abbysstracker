<?php

use yii\db\Migration;

/**
 * Handles the creation of table `target_sites`.
 */
class m180225_152041_create_target_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('target_sites', [
            'id' => $this->primaryKey(),
            'search_query_id' => $this->integer(),
            'domain' => $this->string(256),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('target_sites');
    }
}
