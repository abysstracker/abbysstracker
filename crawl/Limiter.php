<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 26.02.2018
 * Time: 22:25
 */

namespace app\crawl;


class Limiter {

    private $startTime = 0;
    private $maxTimeConnectionDB = 10; //sec

    final public static function checkTime() {

        $currentTime = \Yii::getLogger()->getElapsedTime();
        $maxTime = ini_get('max_execution_time') * 0.1;

        return $currentTime < $maxTime;
    }

    /**
     * Установить время начала выполнения комманд
     */
    public function setStartTime() {
        $this->startTime = microtime(true);
    }

    /**
     * Переоткрыть соединение с базой, если нужно
     * @throws \yii\db\Exception
     */
    public function reopenConnectionWithDB() {

        $currentTime = microtime(true);
        $timeWithRelax = $this->maxTimeConnectionDB * 0.8; //Несколько секунд оставляем для реконнекта.


        if ($currentTime - $this->startTime > $timeWithRelax ) {
            \Yii::$app->db->close();
            \Yii::$app->db->open();
            $this->setStartTime();
        }
    }

}