<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 26.02.2018
 * Time: 22:16
 */

namespace app\crawl;


use app\models\Flags;
use app\models\ParsedSites;
use app\models\Structures;
use yii\helpers\ArrayHelper;
use PHPHtmlParser\Dom;
use yii\log\Logger;


class ParseLink implements Module {
    const PATTERN_CLEAN_DOMAIN = '/^.*?:\/\/.*?(?=\/|$)/';
    const PATTERN_CLEAN_DOMAIN_NEW = '/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/';

    /** @var int id ссылки */
    protected $iParseLinkId = null;
    /** @var string Название таблицы со структурой для работы */
    protected $workTable = '';
    /** @var int id сайта */
    protected $iSitesId = 0;
    /** @var array Ссылки для парсинга */
    private $aLink4Parse = [];
    /** @var int Ссылок за раз на парсинг */
    private $maxLinksParse = 5;
    /** @var ParsedSites */
    private $oParseSite = null;
    /** @var array Статусы ссылок по завершению работы над ними */
    private $aLinksStatus = [];
    /** @var int Результирующий статус работы парсинга над сайтом */
    public $iStatusSite = 0;
    /** @var array Маркировочные флаги */
    protected $aFlags = [];

    protected $aObserveModules = [];

    /**
     * Иницилизация настроек с какой таблицей работать
     */
    public function start() {
        //todo Guzzle выкидывает исключение на 404 надо проверить. Ну что за мудила.
        try {
            //Найдем вначале сайт для работы
            $this->oParseSite = ParsedSites::findOne($this->iSitesId);
            if (is_null($this->oParseSite))
                $this->iStatusSite = Status::ERROR;
            elseif (!$this->checkCanWork())
                $this->iStatusSite = Status::STOP;
            else
                $this->iStatusSite = Status::IN_WORK;

        } catch (\Exception $e) {
            $this->iStatusSite = Status::ERROR;
        }
        $aFlags = Structures::getFlagsFromStructure($this->iSitesId);
        $aFindFlags = Flags::find()->select(['name', 'title'])->where(['name' => $aFlags])->asArray()->all();
        $this->aFlags = ArrayHelper::map($aFindFlags, 'name', 'title');
        foreach ($this->aFlags as $keyFlagColumn => $sFlag) { //todo надо это вынести на уровень модели
            $this->aFlags['flag_'.$keyFlagColumn] = $sFlag;
            unset($this->aFlags[$keyFlagColumn]);
        }
//        echo "status ", $this->iStatusSite;
        $this->notify();
        return $this->iStatusSite;
    }//func

    /**
     * Установить id сайта для парсинга
     * @param $id
     */
    public function setSitesId($id) {

        $this->iSitesId = (int)$id;
    }

    public function beforeExecute() {

        $this->workTable = 'structures_'.$this->oParseSite->id;
        $this->aLink4Parse = Structures::fromTable($this->workTable)
            ->find()
            ->where(['status' => Status::ADD_IN_QUEUE])->limit($this->maxLinksParse)->asArray()->all();

        //todo Воткнуть проверку на существование таблицы со структурой

        if (!count($this->aLink4Parse)) {
            $this->iStatusSite = Status::FINISH;
            $this->notify();
            return $this->iStatusSite;
        }

        $aLinksId = ArrayHelper::getValue($this->aLink4Parse, 'id');

        //Ставим статус в работе ссылкам сразу перед работой
        Structures::fromTable($this->workTable)::updateAll(['status' => Status::IN_WORK], ['id' => $aLinksId]);

        $this->iStatusSite = Status::IN_WORK;
        $this->notify();
        return $this->iStatusSite;
    }

    public function execute() {

        if (!empty($this->aLink4Parse)) {

            foreach ($this->aLink4Parse as $link) {
                try {

                    $this->iParseLinkId = $link['id'];

                    $client = new \GuzzleHttp\Client(['base_uri' => $this->oParseSite->domain]);
                    //todo под кроликом почему-то приложение yii2 будто подыхает и не отрабатывает даже \\Yii::$app->components
                    $res = $client->request('GET', $link['url'], ['http_errors' => true, 'timeout' => 20, 'headers' => ['User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36']]);

                    if ($res->getStatusCode() == 200) {
                        $html = new Dom(); //todo Issue #4: Замена парсера контента
                        $html->load($res->getBody());

                        $aLinks = $html->find('a');
                        $aInputs = $html->find('input');

                        $oCurrentLink = Structures::fromTable($this->workTable)::findOne(['id' => $link['id']]);

                        list ($keywords, $title, $description) = $this->parseMeta($html);
                        $oCurrentLink->seo_keywords = $keywords;
                        $oCurrentLink->seo_title = $title;
                        $oCurrentLink->seo_description = $description;
                        $oCurrentLink->h1 = $this->parseH1($html);

                        $this->setFlags($aLinks, $oCurrentLink, $aInputs);
                        $oCurrentLink->save();
                        //целостность логики хромает, но ср@ки уже горят
                        $this->setUrl($aLinks, $link['url']);

                        $this->aLinksStatus[$link['id']] = Status::FINISH;
                    } else {
                        if ($res->getStatusCode() == 404)
                            $this->aLinksStatus[$link['id']] = Status::NOT_FOUND;
                        else
                            $this->aLinksStatus[$link['id']] = Status::ERROR;
                        //todo Под кроликом не работает логирование...
                        echo $link['url']
                            . ' => Status ' . $this->aLinksStatus[$link['id']]
                            . ' => Staus guzzle ' . $res->getStatusCode(), "\n";
                    }
                    //Пока сюда встроем отдых между парсингом
//                    sleep(mt_rand() / mt_getrandmax()); //Значения от 0 до 1 с 10 знаками после запятой


                } catch (\Exception $e) {
                    $this->aLinksStatus[$link['id']] = Status::ERROR;
                    \Yii::getLogger()->log($e->getMessage(), LOGGER::LEVEL_ERROR, 'crawl');
                    echo $e->getMessage(), "\r";
                }
            }
        } //foreach parse link
    }

    public function afterExecute() {

        foreach ($this->aLinksStatus as $linkId => $status) {
            Structures::fromTable($this->workTable)::updateAll(['status' => $status], ['id' => $linkId]);
        }
    }

    public function complete() {

        //Переведем в статус финиш, если только все ок

        $oParseSite = ParsedSites::findOne($this->iSitesId);
        if (is_null($oParseSite))
            return false;

        if (in_array($oParseSite->status, [Status::STOP, Status::IN_WORK])) {
            $this->iStatusSite = Status::FINISH;
        }
        $this->notify();
    }

    /**
     * Проверить, можем ли мы парсить сайт дальше или он остановлен?
     * @return bool
     */
    public function checkCanWork() {

        $oParseSite = ParsedSites::findOne($this->iSitesId);
        if (is_null($oParseSite)) return false;

        if ($oParseSite->status == Status::IN_WORK
            OR $oParseSite->status == Status::ADD_IN_QUEUE
            OR $oParseSite->status == Status::NOT_PROCESSED
        ) return true;

        return false;
    }

    public function notify() {

        foreach ($this->aObserveModules as $observe)
            $observe->update($this);

    }

    public function registerObserve(ObserveModule $module) {

        foreach ($this->aObserveModules as $observe) {
            if ($observe === $module)
                return false;
        }

        $this->aObserveModules[] = $module;
        return true;
    }


    public function getStatusParseSite() {
        return $this->iStatusSite;
    }


    public function getParseSiteId() {
        return $this->iSitesId;
    }


    public function getParseSiteName() {
        return $this->oParseSite->domain;
    }

    /**
     * подготовка alias_path из ссылки
     * @param $sHref string распарсиваемая ссылка
     * @param $sCurrentUrl string урл с которого происходит парс ссылок
     * @return string
     */
    private function parseAliasPath($sHref, $sCurrentUrl) {

        if (mb_substr($sHref, 0,2) == '//' OR mb_substr($sHref, 0, 4) == 'http') {
            if (mb_substr($sHref, 0,2) == '//')
                $sHref = mb_substr($sHref, 2);
            else {
                $sHref = str_replace('https://', '', $sHref);
                $sHref = str_replace('http://', '', $sHref);
            }

            //Найдем следующий первый слеш и отрежем как хвост, а то регулярка и хвост накрывает
            $slashPos = mb_strpos($sHref, '/', 3, 'UTF-8');
            if ($slashPos) {
                $sTail = mb_substr($sHref, $slashPos);
                $sHref = mb_substr($sHref, 0, $slashPos);
            } else {
                $sTail = '';
            }

            //Экспереминтальная функция на проверку русских доменов и вообще пуникод
            if ($convert = idn_to_ascii($sHref))
                if ($convert2 = idn_to_utf8($convert) != $convert)
                    $sHref = '';

            // '//site.ru/avatar/98/763762.jpg'
            // возвращает /avatar/98/763762.jpg'
            $sHref = preg_replace(self::PATTERN_CLEAN_DOMAIN_NEW, '', $sHref);
            $sHref .= $sTail;
            $sHref = $this->setFirstSlash($sHref);
        }

        //удалим двойные слэши. тройные нахер, может попозже
        $sHref = str_replace('//', '/', $sHref);


        //если нет первого слеша, то возможно неявно домен прописан, проверим еще раз
        if (mb_substr($sHref, 0,1) != '/') {
            $sHrefCompare = $sHref;
            //Найдем следующий первый слеш и отрежем как хвост, а то регулярка и хвост накрывает
            $slashPos = mb_strpos($sHref, '/', 1, 'UTF-8');
            if ($slashPos) {
                $sTail = mb_substr($sHref, $slashPos);
                $sHref = mb_substr($sHref, 0, $slashPos);
            } else {
                $sTail = '';
            }
            //Экспереминтальная функция на проверку русских доменов и вообще пуникод
            if ($convert = idn_to_ascii($sHref))
                if ($convert2 = idn_to_utf8($convert) != $convert)
                    $sHref = '';


            $sHref = preg_replace(self::PATTERN_CLEAN_DOMAIN_NEW, '', $sHref);

            $sHref .= $sTail;
            //Ничего не вырезали, значит домена нет и это просто корявая относительная ссылка
            if ($sHrefCompare == $sHref) {
                $sHref = $sCurrentUrl .  $sHref;
            }
        }
        $sHref = $this->setLastSlash($sHref);
        $sHref = $this->setFirstSlash($sHref);
        return $sHref;

    }

    /**
     * @param $subLink
     * @return bool
     */
    private function isGarbage($subLink)
    {
        return Analyzer::isGarbage($subLink->href, $this->oParseSite->domain);
    }


    /**
     * @param $aLinks
     * @param $oCurrentLink
     * @param $aInputs
     * @throws \Exception
     */
    private function setFlags($aLinks, &$oCurrentLink, $aInputs)
    {
        foreach ($this->aFlags as $keyFlag => $sFlag) {
            foreach ($aLinks as $subLink) {

                if (!$this->isGarbage($subLink)) {
                    //Нашли в ссылках
                    $sTextToSearch = trim($subLink->text);
                    if ($sTextToSearch) {
                        try {
                            if (mb_substr_count($sTextToSearch, $sFlag)) {
                                $oCurrentLink->$keyFlag = 1;
                            }
                        } catch (\Exception $e) {
                            throw $e;
                        }

                    }
                }
                //Прокрутим инпуты
                foreach ($aInputs as $input) {
                    $sTextToSearch = trim($input->text);
                    if ($sTextToSearch) {
                        try {
                            if (mb_substr_count($sTextToSearch, $sFlag)) {
                                $oCurrentLink->$keyFlag = 1;
                            }
                        } catch (\Exception $e) {
                            throw $e;
                        }
                    }
                }
            }

        }

    }

    /**
     * @param \PHPHtmlParser\Dom $html
     * @return string
     */
    private function parseH1($html)
    {
        //Выберем первый найденый заголовок
        $sH1 = '';
        $aH1 = $html->find('h1');

        foreach ($aH1 as $h1Text) {
            $sH1 .= ' ' . $h1Text->text;
        }
        $sH1 = trim($sH1);

        return $sH1;
    }

    private function parseMeta($html)
    {
        $aMeta = $html->find('meta');
        $keywords = '';
        $description = '';
        $title = $html->find('title', 0);

        if (!is_null($title)) {
            $title = $title->text;
        } else {
            $title = '';
        }

        foreach ($aMeta as $meta) {
            if ($meta->name == 'description') {
                $description = $meta->content;
            }

            if ($meta->name == 'keywords') {
                $keywords = $meta->content;
            }
        }

        $aMetaData = [trim(strip_tags($keywords)), trim(strip_tags($title)), trim(strip_tags($description))];
        return $aMetaData;
    }

    /**
     * @param $aLinks
     * @param $sCurrentUrl
     */
    private function setUrl($aLinks, $sCurrentUrl)
    {
        //Пробежимся по найденным ссылкам, проверим есть ли в базе

        foreach ($aLinks as $subLink) {

            if (!$this->isGarbage($subLink)) {

                //обрезаем домен из урла, нужен простой alias_path без домена со слешами сначала и в конце
                $sHref = $this->parseAliasPath($subLink->href, $sCurrentUrl);


                $searchLink = Structures::fromTable($this->workTable)::findOne(['url' => $sHref]);
                //Если нет, то добавить в очередь
                if (is_null($searchLink)) {
                    $newLinkStructure = Structures::fromTable($this->workTable);
                    $newLinkStructure->url = $sHref;
                    $newLinkStructure->status = Status::ADD_IN_QUEUE;
                    $newLinkStructure->is_goods = 0;
                    $newLinkStructure->alias = '';
                    $newLinkStructure->h1 = '';
                    $newLinkStructure->seo_title = '';
                    $newLinkStructure->seo_description = '';
                    $newLinkStructure->seo_keywords = '';
                    $newLinkStructure->save();
                }
            }
        }
    }

    /**
     * @param $sHref
     * @return string
     */
    private function setLastSlash($sHref)
    {
        if (substr($sHref, -1) != '/') {
            $sHref .= '/';
        }
        return $sHref;
    }

    /**
     * @param $sHref
     * @return string
     */
    private function setFirstSlash($sHref)
    {
        if (mb_substr($sHref, 0, 1) != '/') {
            return '/' . $sHref;
        }
        return $sHref;
    }


}//class