<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 27.03.2018
 * Time: 23:07
 */

namespace app\crawl;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class Queue {

    //Максимальное кол-во получателей сообщений
    const MAX_CONSUMER = 1;

    /**
     * Добавить в очередь элемент для пользователя
     * @param string $userId ТОЛЬКО СТРОКА!
     * @param string $data ТОЛЬКО СТРОКА!
     * @return bool
     */
    public static function set($userId = 'user0', $data) {

        $data = (string)$data;

        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare($userId, false, true, false, false);

        $msg = new AMQPMessage($data, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $channel->basic_publish($msg, '', $userId);

        //Если находимся в консоли
        if (is_a(\Yii::$app,'yii\console\Application'))
            echo " [x] Sent for {$userId} - '{$data}'\n";

        $channel->close();
        $connection->close();

        return true;
    }
}