<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 26.02.2018
 * Time: 22:57
 */

namespace app\crawl;

interface Module {

    public function start();

    public function setSitesId($id);

    public function afterExecute();

    public function beforeExecute();

    public function execute();

    public function complete();

    public function notify();

    public function registerObserve(ObserveModule $module);

    public function getStatusParseSite();

    public function getParseSiteId();

    public function getParseSiteName();

}