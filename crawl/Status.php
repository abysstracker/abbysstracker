<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 25.02.2018
 * Time: 22:13
 */

namespace app\crawl;


/**
 * Class Status
 * @package app\crawl
 */
class Status {

    /** Статусы для ссылок */

    const ADD_IN_QUEUE = 1;

    const IN_WORK = 2;

    const ERROR = 3;

    const FINISH = 4;

    const NOT_FOUND = 5;

    const NOT_VALID = 6;

    const NOT_PROCESSED = 7;

    const STOP = 8;
    //Используется для проектов, для ссылок использовать обычный ERROR
    const ERROR_HAS = 9;

    /** Статусы  */


    public static $aStatusList = [
        self::ADD_IN_QUEUE => 'В очереди',
        self::IN_WORK => 'В работе',
        self::ERROR => 'Завершено аварийно',
        self::ERROR_HAS => 'Есть ошибки',
        self::FINISH => 'Завершено корректно',
        self::NOT_FOUND => 'Не найдено',
        self::NOT_VALID => 'Не валидно',
        self::NOT_PROCESSED => 'Добавлено в очередь',
        self::STOP => 'Остановлен'
    ];

    /**
     * Получить Заголовок статуса по его ключу
     * @param $statusId
     * @return string
     */
    public static function getStatusNameById($statusId){

        $statusId = (int)$statusId;
        return isset(self::$aStatusList[$statusId]) ? self::$aStatusList[$statusId] : 'Статус утонул на дно';
    }

    /**
     * Вернуть статусы "в работе"
     * @return array
     */
    public static function getStatusForPlay() {
        return [
            self::ERROR,
            self::STOP
        ];
    }

}//class
