<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 26.02.2018
 * Time: 22:21
 */

namespace app\crawl;

/**
 * Классификатор данных
 * Class Analyzer
 * @package app\crawl
 */
class Analyzer {



//    const GARBAGE_SYMBOL_LIST = [
//        '.jpg',
//        '.jpeg',
//        '.png',
//        '.gif',
//        '?',
//        ':',
//        '#'
//    ];

    /**
     * Проверим ссылку на муссор, если она ведет на внешку или не является ссылкой
     * @param $href
     * @param $domainCompare string домен с которым сравниваем. В нашем случаи это должен быть сайт на который нацелились
     * @return bool
     */
    public static function isGarbage($href, $domainCompare) {

        if (!$href) {
            return true;
        }

        if (self::checkGarbageSymbol($href)){
            return true;
        }

        //Обрежим протоколы сразу
        $domainCompare = str_replace('https://', '', $domainCompare);
        $domainCompare = str_replace('http://', '', $domainCompare);

        $href = str_replace('https://', '', $href);
        $href = str_replace('http://', '', $href);

        //Если это ссылка с автоматическим определением протокола, то тоже уберем эти два слеша
        if (mb_substr($href, 0, 2) == '//') {
            $href = mb_substr($href, 2);
        }


        //Все что находится за первым слешем берем как алиас
        $posistionSlash = mb_strpos($href, '/');
        if ($posistionSlash !==  false) {
            $alias = mb_substr($href, $posistionSlash);
            $aUrl = explode('.', $alias);
            if ($aUrl) {
                if (count($aUrl)> 1) // Есть располовинился, то все
                    return true;
            }
        }

        //Если начинается не со слеша, то проверим на хост и домен
        if (mb_substr($href, 0, 1) != '/') {
            //Проверим, может наш хост
            if (mb_substr($href, 0, mb_strlen($domainCompare)) == $domainCompare) {
                return false;

            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * Проверка ссылку на мусорность по наличию мусорного признака
     * @param $href
     * @return bool
     */
    private static function checkGarbageSymbol($href) {

//        if (!empty(self::GARBAGE_SYMBOL_LIST)) {

            foreach ([
                         '.jpg',
                         '.jpeg',
                         '.png',
                         '.gif',
                         '?',
//                         ':',
                         '#'
                     ] as $sSymbol) {
                if (mb_strpos($href, $sSymbol) !== false) {
                    return true;
                }
            }

//        }

        return false;
    }

/**
 * Тесты
 */
//echo isGarbage('http://sandbox.onlinephpfunctions.com', 'https://askolt.ru') , " http://sandbox.onlinephpfunctions.com\r ";
//echo isGarbage('/fdsafdsa', 'https://askolt.ru') , "/fdsafdsa \r";
//echo isGarbage('/fdsafdsa.jpf', 'https://askolt.ru') , "/fdsafdsa.jpf \r";
//echo isGarbage('/fdsafdsa.', 'https://askolt.ru') , "/fdsafdsa. \r";
//echo isGarbage('/fdsafdsa.1', 'https://askolt.ru') , "/fdsafdsa.1 \r";
//echo isGarbage('/132', 'https://askolt.ru') , "/132 \r";
//echo isGarbage('fdsafdsa', 'https://askolt.ru') , "fdsafdsa \r";
//echo isGarbage('fdsafdsa?', 'https://askolt.ru') , "fdsafdsa? \r";
//echo isGarbage('fdsafdsa?fdsfds=32', 'https://askolt.ru') , "fdsafdsa?fdsfds=32 \r";
//echo isGarbage('fd/safdsa', 'https://askolt.ru') , "fd/safdsa \r";
//echo isGarbage('https://askolt.ru/fd/safdsa', 'https://askolt.ru') , "https://askolt.ru/fd/safdsa \r";
//echo isGarbage('https://askolt.ru', 'https://askolt.ru') , "https://askolt.ru \r";
//echo isGarbage('https://fds.ru/fd/safdsa', 'https://askolt.ru') , "https://fds.ru/fd/safdsa \r";
//echo isGarbage('www.ya.ru', 'https://askolt.ru') , "www.ya.ru \r";
//echo isGarbage('http', 'https://askolt.ru') , "http \r";
//echo isGarbage('//askolt.ru', 'https://askolt.ru') , "//askolt.ru \r";
//echo isGarbage('//sex.com', 'https://askolt.ru') , "//sex.com \r";
//echo isGarbage('askolt.ru', 'https://askolt.ru') , "askolt.ru \r";
//echo isGarbage('', 'https://askolt.ru') , "Пустая строка \r";

}