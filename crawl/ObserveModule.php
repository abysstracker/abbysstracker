<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 02.04.2018
 * Time: 23:32
 */

namespace app\crawl;


interface ObserveModule {


    public function update(Module $module);

}