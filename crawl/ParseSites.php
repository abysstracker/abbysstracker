<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 26.02.2018
 * Time: 23:03
 */

namespace app\crawl;

use app\models\Flags;
use app\models\ParsedSites;
use app\models\SearchQuery;
use app\models\Structures;
use app\models\TargetSites;
use yii\helpers\ArrayHelper;
use yii\web\Application;

class ParseSites implements ObserveModule {


    public function start() {


        //Выберем все сайты которые нужно пропарсить
        $aTargetSites = TargetSites::find()->all();

        foreach ($aTargetSites as $site) {

            list($bValidDomain, $host) = self::prepareDomainToParse($site->domain);
            //Найдем струкруту нацеленного сайта
            $oSite = ParsedSites::findOne(['domain' => $host]);

            if (is_null($oSite)) { //Сохраним в любом случае
                $oSite = new ParsedSites();
                $oSite->last_modified = \Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                $oSite->date_start = \Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                $oSite->domain = $host;
                $oSite->status = $bValidDomain ? Status::NOT_PROCESSED : Status::NOT_VALID;
//                $oSite->parent_site = ''; //todo когда до редиректов доберемся

                if (!$oSite->save()) {

                    var_dump($oSite->getErrors());exit;
//                    throw new \Exception('Не могу сохранить сайт для структуры '); //Вынести исключения
                }
            }

            if (!$bValidDomain)
                continue;

            $bRefreshTableShema = false;

            $oSite = ParsedSites::findOne(['domain' => $host]);
            //Проверим перед созданием что такой таблицы еще нет
            $tableShema = \Yii::$app->db->schema->getTableSchema("structures_{$oSite->id}");
            //Создадим отдельную таблицу под структуру
            if (is_null($tableShema)) {
                $sNewStructure = 'CREATE TABLE `structures_:id` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT ,
                                  `is_goods` int(11) NOT NULL,
                                  `status` int(11) NOT NULL,
                                  `alias` varchar(256) NOT NULL,
                                  `url` varchar(256) NOT NULL,
                                  `h1` varchar(256) NOT NULL,
                                  `seo_title` varchar(256) NOT NULL,
                                  `seo_description` varchar(256) NOT NULL,
                                  `seo_keywords` varchar(256) NOT NULL,
                                  PRIMARY KEY (ID)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

                \Yii::$app->db->createCommand($sNewStructure, ['id' => $oSite->id])->execute();
                \Yii::$app->db->schema->refresh();
                $bRefreshTableShema = true;
            }

            //Теперь добавим маркировачные флаги
            $oSearchQuery = SearchQuery::findOne(['id' => $site->search_query_id]);
            $aFlagsToAlter = [];
            if (!is_null($oSearchQuery)) {//Если не пусто поле флагов
                $aFlags = explode(',', $oSearchQuery->search_words);
                if (count($aFlags)) {
                    foreach ($aFlags as $aFlagItem) {//переберем и сделаем валидными
                        $sFlag = trim($aFlagItem);
                        if ($sFlag) {
                            $sFlag = strip_tags($sFlag);
                            $sFlag = preg_replace("/[^а-яА-Я0-9a-zA-Z ]/iu", "", $sFlag); // очищаем строку от недопустимых символов
                            $sFlag = trim($sFlag);
                            if ($sFlag) {
                                $aFlagsToAlter[] = $sFlag;
                            }
                        }
                    }
                }
            }

            if (count($aFlagsToAlter)) {
                $aFlagsToAlter = array_unique($aFlagsToAlter);
                $aFindFlags = Flags::find()->select(['title'])->where(['title' => $aFlagsToAlter])->asArray()->all();
                $aFindFlags = ArrayHelper::getColumn($aFindFlags, 'title');

                /**
                 * Эти флаги пойдут на добавление в общий список всех флагов в системе
                 */
                $aToInsert = array_diff($aFlagsToAlter, $aFindFlags); //
                if (count($aToInsert)) {
                    $aRowsInsert = [];
                    foreach ($aToInsert as $sFlag) {
                        $aRowsInsert[] = [
                            'id' => 0,
                            'name' => $this->translit($sFlag),
                            'title' => $sFlag
                        ];
                    }

                    //Добавим флаги в общий список
                    \Yii::$app->db->createCommand()->batchInsert(Flags::tableName(), ['id', 'name', 'title'], $aRowsInsert)->execute();
                }

                /**
                 * Теперь добавим флаги как колонки к таблице
                 * но в начале чтобы ошибок дубликатов не было мы сверим с шемой
                 */
                $aFlagsInScheme = Structures::getFlagsColumnStructure($oSite->id);
                $aFindFlags = Flags::find()->select(['name'])->where(['title' => $aFlagsToAlter])->asArray()->all();
                $aFindFlags = ArrayHelper::getColumn($aFindFlags, 'name');
                foreach ($aFindFlags as &$sFlag) {
                    $sFlag = 'flag_'.$sFlag;
                }
                unset($sFlag);
                $aFlagsToAlter = array_diff($aFindFlags, $aFlagsInScheme); //Теперь когда есть данные о флагах выкинем присутствующее
                if (count($aFlagsToAlter)) {
                    //Так как в PDO да и YII2 нет бинда массива, то придется костылить
                    $sNewStructure = 'ALTER TABLE `structures_:id`';
                    foreach ($aFlagsToAlter as $sFlag) {
                        $sNewStructure .= 'ADD '.$sFlag.' int(1) NOT NULL,';
                    }
                    $sNewStructure = rtrim($sNewStructure, ','); //Ох уж эти иновации
                    $sNewStructure .= ';';
                    \Yii::$app->db->createCommand($sNewStructure, ['id' => $oSite->id])
                        ->execute();
                    $bRefreshTableShema = true;
                }
            }

            if ($bRefreshTableShema) {
                \Yii::$app->db->schema->refresh();
            }

//            $aRes = Structures::findFromTable('structures_'.$oSite->id)->all();

            //Закинем ссылку на главную в таблицу стрктур, крон подберет ее и будет парсить сайт
            if (is_null(Structures::fromTable('structures_'.$oSite->id)::findOne(['url' => '/']))) {
                $newStructure = Structures::fromTable('structures_'.$oSite->id);
                $newStructure->url = '/';
                $newStructure->status = Status::ADD_IN_QUEUE;
                $newStructure->is_goods = 0;
                $newStructure->alias = '';
                $newStructure->h1 = '';
                $newStructure->seo_title = '';
                $newStructure->seo_description= '';
                $newStructure->seo_keywords= '';
                $newStructure->save(false);
            }

            //Поставим в очередь сайт, если стоит как не обработанно
            if ($oSite->status == Status::NOT_PROCESSED) {
                //todo Это временно, потом придумаем как это разрулить
                Queue::set('user0', $oSite->id);
                Queue::set('user0', $oSite->id);
                Queue::set('user0', $oSite->id);
                Queue::set('user0', $oSite->id);
                Queue::set('user0', $oSite->id);
                Queue::set('user0', $oSite->id);
                Queue::set('user0', $oSite->id);
                $oSite->status = Status::ADD_IN_QUEUE;
                $oSite->save();
            }
        } //foreach
        return 1;
    }//func

    /**
     * Подготовить домен для парсинга
     * В случаи удачи вернет подготовленный домен,
     * а в случаи провала вернёт тот же url что пришел и изменит флаг валидности на false
     * @param $url
     * @return array Вернёт массив из 2-х элементов, где первый
     */
    public static function prepareDomainToParse($url) {

        $bValidDomain = true;

        //Проверим что за ссылка нам пришла, подготовим ее
        $sUrl = mb_strtolower($url);
        $sUrl = strip_tags($sUrl); //Мало ли
        $aUrl = parse_url($sUrl);

        $scheme = (!empty($aUrl['scheme'])) ? $aUrl['scheme'] : 'http';
        $scheme .= '://';
        $host = (!empty($aUrl['host'])) ? $scheme . $aUrl['host'] : null;

        if (is_null($host)) {
            //Найдем тогда в строке что пришло
            $regexp4SearchDomain = '/([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}/i';
            $count = preg_match_all($regexp4SearchDomain, $url, $match, PREG_PATTERN_ORDER);
            if ($count) {
                $host = $scheme . $match[0][0];
            } else {
                $bValidDomain = false;
                $host = $sUrl;
            }
        }

        return [$bValidDomain,$host];
    }


    public function update(Module $module) {

        $oParseSite = ParsedSites::findOne($module->getParseSiteId());

        if ($oParseSite->status == Status::ADD_IN_QUEUE AND $module->getStatusParseSite() == Status::IN_WORK) {
            $oParseSite->date_start = \Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
        }

        $oParseSite->status = $module->getStatusParseSite();
        $oParseSite->last_modified = \Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
        $oParseSite->save();
    }

    /**
     * Транслитиреация входящих слов в латинские символы.
     * @param $s
     * @return mixed|null|string|string[]
     */
    public function translit($s) {
        $s = (string) $s;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), "", $s);
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s);
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
        $s = preg_replace("/[^а-яА-Я0-9a-zA-Z ]/iu", "", $s); // очищаем строку от недопустимых символов
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = str_replace(" ", "_", $s);
    return $s;
}


}//class

/**
 * Переменные в phpmyadmin
 * Удалил STRICT_TRANS_TABLES
 * sql mode
 * ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
 */