<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'flushInterval' => 1, //Интервал выгрузки для всех логов. Т.к. в консоли процессы бесконечные, то выгружаем сразу
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/logs/error_console.txt',
                    'logVars' => [], //Сюда дополнительно можно приписывать GET, POST,
                    'exportInterval' => 1,
                ],
                [//Дополнительный для паука, чтобы не мешать с общими ошибками
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['crawl'],
                    'logFile' => '@app/logs/crawl_info.txt',
                    'logVars' => [],
                    'exportInterval' => 1,
                ]
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest']
        ],
        'db' => $db,
    ],
    'timeZone' => 'Europe/Moscow',
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
