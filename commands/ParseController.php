<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 22.03.2018
 * Time: 22:25
 */

namespace app\commands;

use app\crawl\Limiter;
use app\crawl\ParseLink;
use app\crawl\ParseSites;
use app\crawl\Status;
use yii\console\Controller;
use yii\console\ExitCode;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use yii\helpers\Console;

/**
 * Запуск демона для выполнения парсинга в фоне
 * Class ParseController
 * @package app\commands
 */
class ParseController extends Controller
{
    //Максимальное кол-во получателей сообщений
    const MAX_CONSUMER = 7;
    /**
     * Вызов парсера из под консоли
     * @param string $user Идентификатор пользователя
     * @return int Exit code
     */
    public function actionIndex($user = 'user0')
    {
        $oLimiter = new Limiter();
        $oLimiter->setStartTime();

//        Формируем каркасы сайтов
        $targetSitesToParse = new ParseSites();
//        $targetSitesToParse->start();

        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();
        list(,,$consumerCount) = $channel->queue_declare($user, false, true, false, false);

        echo " Consumer count " . $consumerCount, "\n";

        if( $consumerCount >= self::MAX_CONSUMER) {
            $channel->close();
            $connection->close();
            echo " Consumer limit " . self::MAX_CONSUMER, "\n";
            $msgExit = $this->ansiFormat(' Exit from script', Console::FG_YELLOW);
            echo $msgExit, "\n";
            return ExitCode::OK;
        }

        echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        $callback = function($msg) use ($oLimiter, $targetSitesToParse, $consumerCount) {
            $oLimiter->reopenConnectionWithDB();
            echo "WORKER " . ($consumerCount + 1) . " [x] Received ", $msg->body, "\n";

            //Парсим ссылки
            $oParseStructure = new ParseLink();
            $oParseStructure->registerObserve($targetSitesToParse);
            $oParseStructure->setSitesId($msg->body);
            if ($oParseStructure->start() != Status::IN_WORK) {
                echo ' Complete Site ' . $oParseStructure->getParseSiteName(), "\n";
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                return true;
            }

            $iMaxCountIteration = 5000; //На всякий, пока еще сырая версия.
            $i = 1;
            while (
                ($oParseStructure->checkCanWork() and $i < $iMaxCountIteration)
                AND $oParseStructure->beforeExecute() == Status::IN_WORK)
            {
                echo "WORKER " . ($consumerCount + 1), "\n";
                $oParseStructure->execute();
                $oParseStructure->afterExecute();
                $oLimiter->reopenConnectionWithDB();
                $i++;
            }
            $oParseStructure->complete();
            echo ' Complete Site ' . $oParseStructure->getParseSiteName(), "\n";
            //Подтверждение о выполненном задании. Не знаю насколько верное это, но вроде работает
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            return true;
        };
        //Метка на справедливое распределение задач, чтобы остальные воркеры не ждали пока задача выполниться
        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($user, '', false, false, false, false, $callback);


        while(count($channel->callbacks)) {
            $channel->wait();

            //todo По завершению и ожиданию новых задач более 30 секунд завершить работу
        }
        $channel->close();
        $connection->close();

        return ExitCode::OK;
    }

    /** На случай если мы хотим из консоли подкинуть еще задач */
    public function actionNewtask() {

        //Формируем каркасы сайтов
        $targetSitesToParse = new ParseSites();
        $targetSitesToParse->start();
    }
}
