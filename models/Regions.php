<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "regions".
 *
 * @property int $id
 * @property string $title
 * @property int $key
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'integer'],
            [['title'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Регион',
            'title' => 'Title',
            'key' => 'Key',
        ];
    }

    /**
     * Получить список для передачи во view
     */
    public static function getListForView() {
        $aRegions = self::find()
            ->select(['id', 'title'])
            ->asArray()
            ->orderBy('title')
            ->all()
        ;

        $aRegionsList = [];

        if (!empty($aRegions)) {

            foreach ($aRegions as $aRegion) {
                if (!empty($aRegion['id'])) {
                    $aRegionsList[$aRegion['id']] = $aRegion['title'];
                }
            }

        }

        return $aRegionsList;
    }

    public static function getKeyOnId($iId) {
        $aKey = self::find()
            ->select(['key'])
            ->where(['id'=>$iId])
            ->asArray()
            ->one()
        ;

        if (!empty($aKey)) {
            return $aKey['key'];
        }

        return false;
    }
}
