<?php

namespace app\models;

use yii\base\Exception;

/**
 * This is the model class for table "engines".
 *
 * @property int $id
 * @property string $engine
 * @property string $title
 */
class Engines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'engines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['engine', 'title'], 'required'],
            [['engine'], 'string', 'max' => 256],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'engine' => 'Engine',
            'title' => 'Title',
        ];
    }

    /**
     * Получить писок engine (поисковиков)
     * @param bool $bAsArray в виде массива
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($bAsArray=false) {
        $oQuery = self::find();

        if ($bAsArray) {
            $oQuery->asArray();
        }

        return $oQuery->all();
    }

    /**
     * Получить список для передачи во view
     */
    public static function getListForView() {
        $aEngines = self::find()
            ->select(['id', 'title'])
            ->asArray()
            ->all()
        ;

        $aEnginesList = [];

        if (!empty($aEngines)) {

            foreach ($aEngines as $aEngine) {
                if (!empty($aEngine['id'])) {
                    $aEnginesList[$aEngine['id']] = $aEngine['title'];
                }
            }

        }

        return $aEnginesList;

    }

    /**
     * @param $iId
     * @return mixed
     * @throws Exception
     */
    public static function getEngineById($iId) {
        $aEngine = self::find()
            ->select(['engine'])
            ->where(['id'=>$iId])
            ->asArray()
            ->one()
        ;

        if (!empty($aEngine['engine'])) {
            return $aEngine['engine'];
        }

        throw new Exception("Запрошен некорректный поисковик");

    }
}
