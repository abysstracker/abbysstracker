<?php

namespace app\models;

use app\crawl\Status;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "structures".
 *
 * @property int $id
 * @property string $alias
 * @property string $url
 * @property string $h1
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property int $is_goods
 * @property int $status
 */
class Structures extends \yii\db\ActiveRecord
{

    const LIMIT_ON_PAGE = 300;
    protected static $sTableName = 'structures';
    const PREPARE_SITE_DELIMITER = "_";

    /**
     * Смена таблица для работы и работа по модели
     * @param $name
     * @return Structures
     */
    public static function fromTable($name)
    {
        self::$sTableName = $name;
        return new self;
    }

    public static function fromTableId($id)
    {
        self::$sTableName = 'structures_' . $id;
        return new self;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return self::$sTableName;
    }

    /**
     * Сбор структуры конкретнного распарсенного сайта
     * @param $iSiteTableId
     * @param $aSelectedFlags
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function getStructureSite($iSiteTableId, $aSelectedFlags = [], $level = 2, $page = 1, $sBranchUrl) {


        $aStructureFlags = self::getFlagsFromStructure($iSiteTableId);
        //Выкинем несуществующие флаги для текущей структуры из запроса
        $aSelectedFlags = array_filter($aStructureFlags, function($sFlag) use ($aSelectedFlags) {
            return in_array($sFlag, $aSelectedFlags);
        }) ;
        $aSelectedFlags = array_unique($aSelectedFlags);

        $aStructure = self::fromTable('structures_' . $iSiteTableId)::find();
        //Добавим в запрос нужные флаги
        foreach ($aSelectedFlags as $sFlagName) {
            $aStructure = $aStructure->andWhere(['flag_'.$sFlagName => 0]); //Метод исключения
        }


        $aStructure = $aStructure->orderBy('url')
//            ->limit(1000)
            ->where(['like', 'url', $sBranchUrl . '%/', false])
//            ->offset($page)
            ->asArray()
            ->all();

        $level = substr_count($sBranchUrl, '/') + 1;

//        $oDataProvider = new ArrayDataProvider([
//            'allModels' => $aStructure,
//            'pagination' => [
//                'pageSize' => self::LIMIT_ON_PAGE,
//            ]
//        ]);
//        $aStructure = $oDataProvider->getModels();
        self::parseDataSite($iSiteTableId, $aStructure, $level);

        return $aStructure;

//        $oDataProvider->setModels($aStructure);
//        return $oDataProvider;
    }


    /**
     * Добавляет ['level' => уровень вложенности] в таблицу структур
     * @param $aStructure
     */
    private static function parseLevel(&$aStructure, $level) {
        if (!empty($aStructure)) {
            foreach ($aStructure as $key => &$aRow) {
                if (!empty($aRow['url'])) {
                    $aRow['level'] = substr_count($aRow['url'], '/');
                    if ($aRow['level'] > $level) {
                        unset($aStructure[$key]);
                    }
                }
            }
        }
    }

    public static function getDataList($iSiteTableId, $aList = []) {

        $oStructure = self::fromTable('structures_' . $iSiteTableId)::find();
        if (!empty($aList)) {
            $oStructure = $oStructure->where(['id' => $aList]);
        }
        $aStructure = $oStructure
            ->orderBy('url')
            ->asArray()
            ->all()
        ;

        if (!empty($aStructure)) {

            self::parseAlias($aStructure);
            self::parseParent($aStructure);
//            ArrayHelper::multisort($aStructure, 'url', SORT_DESC);
        }

        return $aStructure;
    }

    public static function getAllList($iSiteTableId, $aList = []) {
        $aStructure = [];

        if (!empty($aList)) {
            $structure = self::fromTable('structures_' . $iSiteTableId)::find();
            if (!empty($aList)) {
                $structure = $structure->where(['id' => $aList]);
            }
            $structure
                ->asArray()
                ->all()
            ;
            return $aStructure;
        }

        if (!empty($aStructure)) {
            self::parseParent($aStructure);
            self::parseAlias($aStructure);
        }

        ArrayHelper::multisort($aStructure, 'url');

        return $aStructure;
    }

    private static function parseDomain(&$aStructure, $iPostfix) {
        if (!empty($aStructure)) {
            $oParsedSite = ParsedSites::find()->where(['id' => $iPostfix])->one();
            if (!is_null($oParsedSite)) {
                foreach ($aStructure as &$aRow) {
                    $aRow['domain'] = $oParsedSite->domain;
                }
            }
        }
    }



    /**
     * Проставляем parent в структуру
     * @param $aStructure
     */
    protected static function parseParent(&$aStructure) {

        foreach ($aStructure as &$aSection) {
            if ($aSection['url']) {
                $aSection['parent'] = '';

                //получить url родителя, не главной страницы
                if (($aSection['url']) != '/') {
                    $sParentUrl = str_replace($aSection['alias'] . '/', '', $aSection['url']);

                    foreach ($aStructure as $numStr => &$aSectionSearch) {
                        if ($sParentUrl == $aSectionSearch['url']) {
                            $aSection['parent'] = $numStr + 2;
                            continue;
                        }
                    }

                }
            }

        }
        return $aStructure;

    }

    /**
     * @param $sDomain
     * @return int|null|\yii\db\ActiveRecord
     */
    public static function getIdOnDomain($sDomain) {
        $iId = ParsedSites::getIdOnDomain($sDomain);
        return $iId;
    }

    /**
     * Парсинг алиаса на основе урла
     * @todo вынести все вариации проверок в отдельные методы
     * @param $aStructure
     */
    public static function parseAlias(&$aStructure) {
        if (!empty($aStructure)) {

            foreach ($aStructure as &$aRow) {
                $aRow['alias'] = '';

                //если урл есть и он длинее одного символа
                // т.е. не главная страница
                if (!empty($aRow['url']) && strlen($aRow['url']) > 1) {
                    $aUrl = explode('/', $aRow['url']);

                    //если url оканчивается не на "/" и в нем нет гет параметров
                    //то берем то что идет от последнего "/" до конца строки
                    if (substr($aRow['url'], -1) != '/' && !substr_count($aRow['url'], '?')) {
                        $aRow['alias'] = $aUrl[count($aUrl)-1];
                    }

                    //если url оканчивается на "/" и слешей больше одного (не главная)
                    //то забераем значение между последним и предпоследним слешами
                    else if (substr($aRow['url'], -1) == '/' && substr_count($aRow['url'], '/') > 1) {
                        $iAliasKey = count($aUrl)-2;
                        if (!empty($aUrl[$iAliasKey])) {
                            $aRow['alias'] = $aUrl[$iAliasKey];
                        }

                    //если есть гет параметры, начало которых не разделено "/"
                    //то берем от последнего "/" до "?"
                    } else if (substr_count($aRow['url'], '?')) {
                        foreach ($aUrl as $key => $value) {
                            if (substr_count($value, '?') && $key > 0) {
                                $aElement = explode('?', $value);
                                $aRow['alias'] = $aElement[0];
                            }
                        }

                    //если есть гет параметры, начало которых разеделено "/"
                    //то берем то, что перед последним "/" идущим до "?"
                    } else if (substr_count($aRow['url'], '/?')) {
                        foreach ($aUrl as $key => $value) {
                            //проверка на случай, если строка начинается со ?
                            if (substr_count($value, '?') && $key > 0) {
                                $aRow['alias'] = $aUrl[$key - 1];
                            }
                        }
                    }

                }
            }
        }
    }


    public static function getDataBrief($structre_id) {
        return Yii::$app->db->createCommand(
            "SELECT `status`, COUNT(`status`) AS 'count' FROM `structures_$structre_id` GROUP BY `status`;"
        )->queryAll();
    }

    private static function parseStatus(&$aStructure) {
        foreach ($aStructure as &$domain) {
            if (!empty($domain['status'])) {
                $domain['status'] = Status::getStatusNameById($domain['status']);
            }
        }
    }

    /**
     * @param $iSiteTableId
     * @param $aStructure
     * @return mixed
     */
    private static function parseDataSite($iSiteTableId, &$aStructure, $level)
    {
        self::parseStatus($aStructure);
        self::parseLevel($aStructure, $level);
        self::parseDomain($aStructure, $iSiteTableId);
        self::parseAlias($aStructure);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['parsed_site_id', 'alias', 'url', 'h1', 'seo_title', 'seo_description', 'seo_keywords', 'is_goods', 'status'], 'required'],
            [[/*'parsed_site_id',*/ 'is_goods', 'status'], 'integer'],
            [['alias', 'url', 'h1', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
//            'parsed_site_id' => 'Parsed Site ID',
            'alias' => 'Alias',
            'url' => 'Url',
            'h1' => 'H1',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'is_goods' => 'Is Goods',
            'status' => 'Status',
        ];
    }

    public function getLinks4Parse($limit = 1)
    {

    }

    /**
     * Получить одноуровневневый список флагов для текущей структуры без префикса flag_
     * @param $iSiteId
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getFlagsFromStructure($iSiteId) {

        $oStructureColumns = Structures::fromTableId($iSiteId)::getTableSchema()->getColumnNames();
        $aFlagsSelect = [];
        foreach ($oStructureColumns as $key => $sName) { //Сейчас просто вычислим флаги, а имена уже получим из запроса
            if (substr($sName, 0, 4) == 'flag') {
                $aFlagsSelect[] = substr($sName, 5); //Вырежем префикс flags_
            }
        }
        return $aFlagsSelect;
    }

    /**
     * Получить одноуровневневый список колонок флагов для текущей структуры
     * @param $iSiteId
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getFlagsColumnStructure($iSiteId) {

        $oStructureColumns = Structures::fromTableId($iSiteId)::getTableSchema()->getColumnNames();
        $aFlagsSelect = [];
        foreach ($oStructureColumns as $key => $sName) {
            if (substr($sName, 0, 4) == 'flag') {
                $aFlagsSelect[] = $sName;
            }
        }
        return $aFlagsSelect;
    }



    public function save($runValidation = true, $attributeNames = null)
    {
        /**
         * Временное решение на проставление дефолнтных значений флагам, а то везде это вставлять...
         */
        $oStructureColumns = $this->getTableSchema()->getColumnNames();
        $aFlagsSelect = [];
        foreach ($oStructureColumns as $key => $sName) {//todo нужно два метода где один выдает колонки с флагами, второй чистые данные
            if (substr($sName, 0, 4) == 'flag') {
                $aFlagsSelect[] = $sName;
            }
        }
        if (count($aFlagsSelect)) {
            foreach ($aFlagsSelect as $keyFlag => $sFlag) { //Начальные значения для флагов
                if (!$this->$sFlag) {
                    $this->$sFlag = 0;
                }
            }
        }
        return parent::save($runValidation, $attributeNames);
    }
}

