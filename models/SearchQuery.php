<?php

namespace app\models;
use app\crawl\Status;
use yii;


/**
 * This is the model class for table "search_query".
 *
 * @property int $id
 * @property string $search_text
 * @property int $engine_id
 * @property int $status
 * @property string $date_start
 * @property string $region_id
 * @property string $search_words
 */
class SearchQuery extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_query';
    }

    /**
     * @param $task_id
     * @return string
     */
    public static function getSearchTextOnId($task_id) {
        $aSearchText = self::find()->select(['search_text'])->where(['id' => $task_id])->asArray()->one();
        if (is_null($aSearchText['search_text'])) {
            if ($task_id) { //Если искали по правильному ключу, но не нашли
                return "<span style=\"color:#18a218\">{$task_id}</span>";  //Разукрасим, типо системный, все дела
            }
            //Для пустого $task_id
            return '<span style="color:#18a218">Общий список сайтов</span>';  //Разукрасим, типо системный, все дела

        }
        $sSearchText = $aSearchText['search_text'];
        return $sSearchText;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['engine_id', 'status', 'region_id'], 'integer'],
            [['date_start'], 'safe'],
            [['search_words'], 'safe'],
            [['search_text'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'engine_id' => 'Поисковик',
            'status' => 'Статус',
            'search_text' => 'Текст запроса',
            'date_start' => 'Дата начала',
            'column_string_parsed' => 'Кол-во обработанных ссылок',
            'region_id' => 'Регион',
            'search_words' => 'Слова для маркировки',
        ];
    }

    /**
     * Запись таска в базу
     * @param $aData
     * @return bool
     */
    public function setTask($aData) {

        if ($this->load($aData) && $this->validate()) {

            try {
                $this->date_start = \Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                $this->save();
                return $this->id;

            } catch (\PDOException $oException) {
                throw $oException;
            }

        }

    }

    /**
     * @param $iId
     * @return null|\yii\db\ActiveRecord
     */
    public static function getOneById($iId) {
        return self::find()->where(['id' => $iId])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getList() {
        return self::find()->orderBy(['id' => SORT_DESC, 'date_start' => SORT_DESC]);
    }

    /**
     * @throws \Exception
     */
    public function getEngineTitle() {
        try {
            $aEngine = Engines::find()
                ->select(['title'])
                ->where(['id' => $this->engine_id])
                ->asArray()
                ->one()
            ;

        } catch (\Exception $e) {
            throw $e;
        }

        if (!empty($aEngine['title'])) {
            return $aEngine['title'];
        }
    }

    public function setStatusAdd() {
        $this->status = Status::ADD_IN_QUEUE;
        return $this->save();
    }

    /**
     * Удаление таски по id
     * @param $task_id
     * @throws \Throwable
     * @throws \Exception
     * @throws yii\db\StaleObjectException
     */
    public static function removeById($task_id) {
        $oTask = self::find()->where(['id' => $task_id])->one();

        if (!empty($oTask)) {
            try {
                $oTask->delete();
                TargetSites::deleteAll(['search_query_id' => $task_id]);

            } catch (yii\db\StaleObjectException $e) {
                throw $e;
            } catch (\Exception $e) {
                throw $e;
            } catch (\Throwable $e) {
                throw $e;
            }

        }
    }
}
