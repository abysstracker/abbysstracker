<?php

namespace app\models;

use app\crawl\ParseSites;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "target_sites".
 *
 * @property int $id
 * @property string $domain
 * @property int $search_query_id
 */
class TargetSites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'target_sites';
    }

    /**
     * @deprecated
     * @param bool $task_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getDomainList($task_id=false) {
        if ($task_id) {
            return self::find()
                ->where(['search_query_id' => $task_id])
                ->asArray()
                ->all()
            ;
        }
        return self::find()
            ->asArray()
            ->all()
        ;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain', 'search_query_id'], 'required'],
            [['search_query_id'], 'integer'],
            [['domain'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'domain' => 'Domain',
            'search_query_id' => 'Search Query ID',
        ];
    }

    /**
     * @param $aDomainList
     * @param $iTaskId
     * @throws Exception
     */
    public static function insertDomainList($aDomainList, $iTaskId) {
        $transaction = Yii::$app->db->beginTransaction();
        $oTask = SearchQuery::getOneById($iTaskId);

        try {
            foreach ($aDomainList as $sDomain) {
                list(,$sDomain) = ParseSites::prepareDomainToParse($sDomain);
                $oTargetSites = new TargetSites();
                $oTargetSites->search_query_id = $iTaskId;
                $oTargetSites->domain = $sDomain;
                $oTargetSites->save();
            }

            $oTask->setStatusAdd();
            $transaction->commit();

        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }

    }


    public static function getIdOnDomain($aDomainList = []) {
        $aResult = [];
        $aIdList = self::find()
            ->select('id')
            ->where(['IN', 'domain', $aDomainList])
            ->asArray()
            ->all()
        ;
        if (!empty($aIdList)) {
            foreach ($aIdList as $aId) {
                if (!empty($aId['id'])) {
                    $aResult[] = $aId['id'];
                }
            }
        }

        return $aResult;
    }

    /**
     * Получить все связаные задачи с доменом
     * @param $sDomain
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getListSearchIdByDomain($sDomain) {

        return self::find()
            ->select('search_query_id')
            ->where(['domain' => $sDomain])
            ->asArray()
            ->all();
    }

    /**
     * Получить список доменов и статусы к ним
     * @param $aId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getListDomainBySearchId($aId) {

        return self::find()
            ->select([self::tableName().'.domain', 'search_query_id', 'status'])
            ->where(['IN', 'search_query_id', $aId])
            ->leftJoin(ParsedSites::tableName(), ParsedSites::tableName().'.domain='.self::tableName().'.domain')
            ->asArray()
            ->all();
    }
}
