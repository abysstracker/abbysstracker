<?php

namespace app\models;

use app\crawl\ParseSites;
use app\crawl\Status;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "parsed_sites".
 *
 * @property int $id
 * @property string $domain
 * @property string $parent_site
 * @property string $date_start
 * @property string $last_modified
 * @property string $status
 */
class ParsedSites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parsed_sites';
    }

    public static function getIdOnDomain($sDomain) {
        $iId = self::find()
            ->select(['id'])
            ->where(['domain' => $sDomain])
            ->asArray()
            ->one()
        ;

        return $iId;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain', /*'parent_site',*/ 'date_start', 'last_modified'], 'required'],
            [['date_start', 'last_modified'], 'safe'],
            [['status'], 'integer'],
            [['domain', 'parent_site'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'domain' => 'Domain',
            'parent_site' => 'Parent Site',
            'date_start' => 'Date Start',
            'last_modified' => 'Last Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getList() {
        return self::find()->orderBy('date_start');
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $bRes = parent::save($runValidation, $attributeNames);
        /**
         * Найдем задачи связанные с сайтом и чекним статус, в зависимости от статуса поменяем статус проекту
         */
        $aSearchId = TargetSites::getListSearchIdByDomain($this->domain);
        $aSearchGroup = TargetSites::getListDomainBySearchId(ArrayHelper::getColumn($aSearchId, 'search_query_id'));
        $aSearchGroup = ArrayHelper::index($aSearchGroup, 'domain', 'search_query_id');
        $bAllFinished = true;
        $bSetStatus = false;

        foreach ($aSearchGroup as $keyProject => $aItem) {

            foreach ($aItem as $keyDomain => $aDomain) {
                //Для случаев когда еще структуры нет и статус NULL
                if (is_null($aDomain['status'])) {
                    $oProject = SearchQuery::findOne(['id'=>$keyProject]);
                    if (!is_null($oProject)) {
                        $oProject->status = Status::NOT_PROCESSED;
                        $oProject->save();
                        $bSetStatus = true;
                        break;
                    }
                }

                //Один из статусов не остановлен или не завершился
                if (!in_array($aDomain['status'], [Status::FINISH, Status::STOP]))
                    $bAllFinished = false;

                if (in_array($aDomain['status'], [Status::ADD_IN_QUEUE, Status::NOT_PROCESSED]))
                    $bAllFinished = false;

                //Один из доменов с ошибкой, проект завершился с ошибкой
                if (in_array($aDomain['status'], [Status::ERROR, Status::NOT_FOUND, Status::NOT_VALID])) {
                    $oProject = SearchQuery::findOne(['id'=>$keyProject]);
                    if (!is_null($oProject)) {
                        $oProject->status = Status::ERROR_HAS;
                        $oProject->save();
                        $bSetStatus = true;
                        break;
                    }
                }
                //Кто-то еще в работе
                if (in_array($aDomain['status'], [Status::ADD_IN_QUEUE, Status::IN_WORK])) {
                    $oProject = SearchQuery::findOne(['id'=>$keyProject]);
                    if (!is_null($oProject) and $oProject->status != Status::IN_WORK) {
                        $oProject->status = Status::IN_WORK;
                        $oProject->save();
                        $bSetStatus = true;
                        break;
                    }
                }
            }
            if (!$bSetStatus) {
                $oProject = SearchQuery::findOne(['id'=>$keyProject]);
                //Если не установлен ранее статус проекту
                if ($bAllFinished) {
                    if (!is_null($oProject)) {
                        $oProject->status = Status::FINISH;
                        $oProject->save();
                    }
                } else {
                    if (!is_null($oProject)) {
                        $oProject->status = Status::ADD_IN_QUEUE;
                        $oProject->save();
                    }
                }
            }
        }
        return $bRes;
    }

}
