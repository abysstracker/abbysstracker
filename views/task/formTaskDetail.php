<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 08.03.2018
 * Time: 1:11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @property $dataProvider
 */

?>

<?= GridView::widget([
    'dataProvider' => $oDataProvider,
    'columns' => [
//        ['class' => 'yii\grid\CheckboxColumn',
//            'checkboxOptions' =>
//                function ($model) {
//                    return ['value' => $model['domain']];
//                }
//        ],
        [
            'format' => 'html',
            'value' => function($model) {
                return Html::a('', ['task/show-structure-list', 'id' => $model['id']], ['class' => 'glyphicon glyphicon-list-alt']);
            }
        ],
        'domain',
        'status_message',
        'date_start',
        'last_modified',
        [
            'format' => 'raw',
            'value' => function($model) {

                $sStyle = 'color:grey;';
                if ($model['was_stopped']) {
                    $sStyle = 'color:black;cursor:pointer;';
                }

                $buttonPlay = Html::tag('p', '', [
                    'class' => 'glyphicon glyphicon-play js-play',
                    'title' => 'Добавить на парсинг',
                    'data-stopped' => (int)$model['was_stopped'],
                    'data-id' => $model['id'],
                    'style' => $sStyle
                ]);

                $sStyle = 'color:black;cursor:pointer;';
                if ($model['was_stopped']) {
                    $sStyle = 'color:grey;';
                }

                $buttonStop = Html::tag('p', '', [
                    'class' => 'glyphicon glyphicon-stop js-stop',
                    'title' => 'Добавить на парсинг',
                    'data-stopped' => (int)$model['was_stopped'],
                    'data-id' => $model['id'],
                    'style' => $sStyle
                ]);

                $sStyle = 'cursor:pointer';
                $buttonRepeat = Html::tag('p','', [
                    'class' => 'glyphicon glyphicon-repeat js-repeat',
                    'data-id' => $model['id'],
                    'title' => 'Начать занового',
                    'style' => $sStyle
                ]);

                $aButtons = [$buttonPlay, $buttonStop, $buttonRepeat];


                return implode('', $aButtons);
            }
        ]
    ]
]); ?>

<div id="js-dialog-repeat" title="Рестарт" style="display: none">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;">x</span>Результат будет сброшен, Вы уверены что хотите перезапустить процесс?</p>
</div>
