<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var array $engine_list список поисковиков
 * @var bool $form_send_success статус отправки формы
 * @var \app\models\SearchQuery $model_sf модель SearchQuery
 */

$this->title = 'Создать проект';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(

        ([
            'action' => '/task/parse-domain-list',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ])
    ); ?>
    <p>Пожалуйста, заполните следующие поля, для создания запроса на сбор структуры:</p>

    <?= $form->field($model_sf, 'search_text')->textInput(['autofocus' => true]); ?>
    <br>
    <p>Введите список урлов, разделенными точкой с запятой, без слеша на конце</p>
    <em>Например:</em>
    <b>http://test.ru; https://example.com</b>
    <div class="form-group field-searchquery-search_words">
        <br>
        <label class="col-lg-1 control-label" for="searchquery-search_words">Ссылки для обработки</label>
        <div class="col-lg-3">
            <?= Html::textarea('domain_list', '', ['placeholder'=>'http://example.com', 'class' => 'form-control']) ?>
        </div>
        <div class="col-lg-8"><p class="help-block help-block-error "></p></div>
    </div>
    <br>

    <p>Не более 30 слов для поиска</p>
    <?= $form->field($model_sf, 'search_words')->textarea(['value' => 'купить']); ?>
    <p>Рекомендуемые слова</p>
    <div style="border: 1px solid black; padding: 10px;">еще, ещё, отложить, купить, заказать, предзаказ, в корзину, <br>
        оформить, следить, попробовать, хочу, поделиться, добавить в корзину, в корзину, оплата и доставка, характеристики,
        рассказать друзьям, состав, выбрать, <br>
        тест, проверить результат, зарегестрироваться
    </div>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>