<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 08.03.2018
 * Time: 1:11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/**
 * @property $dataProvider
 */

$this->title = 'Список доменов по запросу "' . $query_text . '"';

?>

<h1>Список доменов по запросу "<?= $query_text; ?>"</h1>


<?php //$form = ActiveForm::begin([
//    'action' => '../show-structure-list'
//]); ?>
<?php if (!empty($countList)): ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Кол-во страниц</th>
            <th scope="col">Дата последней обработки</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $countList['allPage'] ?></td>
            <td><?= $countList['last_modified'] ?></td>
        </tr>
        </tbody>
    </table>
<?php endif; ?>


<style>
    .glyphicon {
        margin-top: 5px;
        margin-bottom: 10px;
        font-size: 21px;
        color: black;
    }
</style>

<?//= Html::submitButton('ПОКАЗАТЬ СТРУКТУРУ') ?>
<?php Pjax::begin(['id' => 'statuses']) ?>
<?php include 'formTaskDetail.php'; ?>
<?php Pjax::end() ?>
<?//= Html::submitButton('ПОКАЗАТЬ СТРУКТУРУ') ?>
<?php //ActiveForm::end() ?>

