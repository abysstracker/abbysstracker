<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 08.03.2018
 * Time: 1:11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @property $dataProvider
 */

$this->title = 'Подготовка структуры к выгрузке';

?>

<?php $form = ActiveForm::begin([
    'action' => 'save-domain-list?task_id='.$task_id
]); ?>
<?= Html::submitButton('Создать') ?>
<?= GridView::widget([
    'dataProvider' => $data_provider,
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' =>
                function ($model) {
                    if (empty($model['domain'])) {
                        return ['disabled' => true];
                    }
                    return ['value' => $model['domain']];
                }
        ],
        [
            'label' => 'resultHtml',
            'format' => 'raw',
            'value' => function($data){
                return Html::decode($data['resultHtml']);
            },
        ],
    ]

]); ?>



<?= Html::submitButton('Создать') ?>
<?php ActiveForm::end() ?>

