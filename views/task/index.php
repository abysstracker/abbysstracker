<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'Сбор структуры сайта';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Трекер запросов!</h1>
    </div>

    <div class="col-md-3">
        <a href="/task/create-query" class="btn btn-block btn-lg btn-success">Новый проект по поиску</a>
    </div>

    <div class="col-md-3">
        <a href="/task/create-url/" class="btn btn-block btn-lg btn-success">Новый проект через ссылки</a>
    </div>

    <div class="col-md-2">
        <a href="/task/show/" class="btn btn-block btn-lg btn-success">Список сайтов</a>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Запрос</th>
                        <th>Поисковик</th>
                        <th>Дата/время начала</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                        <?= ListView::widget([
                            'dataProvider' => $data_provider,
                            'options' => [
                                'tag' => 'div',
                                'class' => 'task-list',
                                'id' => 'task-list',
                            ],
                            'layout' => "{pager}\n{items}\n{summary}",
                            'itemView' => 'listTaskBody.php',
                            'summary' => 'Показано {begin}-{end} из {totalCount}',

                        ]);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
