<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 08.03.2018
 * Time: 1:11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @property $dataProvider
 * @property $aListValFromSession
 * @property $iTotalRow
 * @property $aStatuses
 */

$this->title = 'Подготовка структуры к выгрузке';

?>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Статус</th>
            <th scope="col">Количество</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($aStatuses as $status) { ?>
            <tr>
                <td><?= $status['status'] ?></td>
                <td><?= $status['count'] ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td><b>Всего</b></td>
            <td><?= $iTotalRow ?></td>
        </tr>
        </tbody>
    </table>

    <a href="export-structure?all=1&id=<?= $iStructureId; ?>">Экспортировать целиком</a> |||
    <a target="_blank" href="export-structure?id=<?=$iStructureId?>">Экспорт стркутуры</a>
    <!-- the treeData root element -->
    <ul id="treeData" data-csrf="<?= $_csrf ?>" data-id="<?= $iStructureId ?>">
        <item class="item" :model="treeData"></item>
    </ul>

    <script src="https://unpkg.com/vue"></script>
    <!-- item template -->
    <script type="text/x-template" id="item-template">
        <li class="list-group-item " v-bind:style="{background: rgbColor}">
<!--        <span class="badge">({{model.count}})</span>-->
            <input class="js_check" type="checkbox"
                   v-bind:checked="checked"
                   v-bind:value="model.id"
                   v-bind:data-url="model.url"
                   v-bind:data-level="model.level"
                   v-bind:data-unik-key="model.id"
            >

            {{model.status}}
            <span @click="toggle">
            <div class="panel panel-default">
                 <div class="panel-heading">{{ model.h1 }}</div>
                 <div class="panel-body">
                     <a target="_blank" v-bind:href="fullUrl">{{model.url}}</a>
                 </div>

                  <table class="table">
                      <thead>
                        <tr>
                            <th>Seo заголовок</th>
                            <th>Seo описание</th>
                            <th>Ключевые слова</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>{{model.seo_title}}</td>
                            <td>{{model.seo_description}}</td>
                            <td>{{model.seo_keywords}}</td>
                        </tr>
                      </tbody>
                      </table>
            </div>
            </span>
            <ul v-show="open" class="list-group" v-if="isFolder">
                <item
                        class="item list-group-item"
                        v-for="(model, index) in model.children"
                        :key="index"
                        :model="model">
                </item>
            </ul>
        </li>
    </script>

    <script>

        var tree = '<?= $tree ?>';
        tree = JSON.parse(tree);
        data = tree.data[0];


        // define the item component
        Vue.component('item', {
            template: '#item-template',
            props: {
                model: Object
            },
            data: function () {
                return {
                    download: false,
                    open: false
                }
            },
            computed: {
                isFolder: function () {
                    return true;
                    return this.model.children &&
                        this.model.children.length
                },
                rgbColor: function() {
                    let hue = 98 - this.model.level;
                    let color = 100 + this.model.level * 10;
                    return 'hsl('+ color + ', 80%,'+ hue + '%)';
                },
                checked: function() {
                    return this.model.checked;
                },
                fullUrl: function () {
                    return this.model.domain + this.model.url;
                }
            },
            methods: {
                toggle: function () {
                    if (this.isFolder) {
                        this.open = !this.open;
                        if (!this.model.download) {
                            Vue.set(this.model, 'download', true);
                            if (!this.model.children)
                                Vue.set(this.model, 'children', []);

                            let params = new URLSearchParams();
                            console.log(this.model);
                            params.append('domain', document.querySelector('#treeData').dataset.id);
                            params.append('branch', this.model.id);
                            params.append('level', this.model.level + 1);
                            params.append('page', 1);
                            params.append('flags', []);
                            params.append('_csrf', document.querySelector('#treeData').dataset.csrf);

                            fetch('/task/render-tree/', {
                                method: 'POST',
                                body: params
                            }).then((response) => {
                                if (response.ok) {
                                    return response.json();
                                }
                            }).then((json) => {
                                for (item in json.data) {
                                    this.model.children.push(json.data[item]);
                                }
                            });
                        }
                        this.download = true;
                    }
                },
            }
        });

        // boot up the treeData
        var treeData = new Vue({
            el: '#treeData',
            data: {
                treeData: data,
              },
            methods: {
                exportTree: function() {
                    console.log(this.treeData);
                }
            }
        })
    </script>




<?php //$form = ActiveForm::begin([
//    'action' => 'export-structure?id=' . $iStructureId
//]); ?>
<?//= Html::submitButton('Экспортировать выбранное') ?>
<!---->


<?//= GridView::widget([
//    'dataProvider'=>$dataProvider,
//    'columns' => [
//        ['class' => 'yii\grid\CheckboxColumn',
//            'checkboxOptions' =>
//                function ($model) {
//                    //@todo Чтение сессии во вьюхе...
//                    $aListValFromSession = Yii::$app->getSession()->get('list_on_export', []);
//                    $bChecked = in_array($model['id'], $aListValFromSession);
//
//                    return [
//                        'class' => 'js_check',
//                        'value' => $model['id'],
//                        'data-level' => $model['level'],
//                        'data-url' => $model['url'],
//                        'data-unik-key' => $model['id'],
//                        'checked' => $bChecked,
//                    ];
//                }
//        ],
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{cur_level} {low_level} {cur_branch}',
//            'header' => "Массовое управление",
//            'buttons' => [
//                'cur_level' => function ($url, $model) {
//                    return Html::tag('p', 'Текущий уровень', [
//                            'class' => 'js_cur_level btn btn-success btn-xs glyphicon glyphicon-resize-horizontal',
//                            'data-url' => $model['url'],
//                            'data-level' => $model['level'],
//                            'data-unik-key' => $model['id'],
//                        ]);
//                },
//                'low_level' => function ($url, $model) {
//                    return Html::tag('div','Дочерние', [
//                            'class' => 'js_low_level btn btn-success btn-xs glyphicon glyphicon-arrow-down',
//                            'data-url' => $model['url'],
//                            'data-level' => $model['level'],
//                            'data-unik-key' => $model['id'],
//                        ]);
//                },
//                'cur_branch' => function ($url, $model) {
//                    return Html::tag('div','Вся ветка', [
//                            'class' => 'js_cur_branch btn btn-success btn-xs glyphicon glyphicon-tree-deciduous',
//                            'data-url' => $model['url'],
//                            'data-level' => $model['level'],
//                            'data-unik-key' => $model['id'],
//                        ]);
//                },
//            ],
//        ],
//        'domain',
//        'level',
//        'status',
//        'url',
//        'alias',
//        'h1',
//        'seo_title',
//        'seo_description',
//        'seo_keywords',
//    ]
//]); ?>
<?//= Html::submitButton('Экспортировать в файл') ?>
<?php //ActiveForm::end() ?>