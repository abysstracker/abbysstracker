<?php
use app\crawl\Status;
use yii\helpers\Html;

/** @var \app\models\SearchQuery $model */
?>


<tr <?php if ($model->status == Status::FINISH): ?>
        class="success"
    <?php elseif ($model->status == Status::ERROR): ?>
    class="danger"
<?php endif; ?>>
    <td><?= $model->id; ?></td>
    <td><a href="/task/show/<?= $model->id ?>"><?= Html::encode($model->search_text); ?></a></td>
    <td><?= $model->getEngineTitle(); ?></td>
    <td><?= $model->date_start; ?></td>
    <td>
        <?php if (isset(Status::$aStatusList[$model->status])): ?>
            <?= Status::$aStatusList[$model->status]; ?>
        <?php else: ?>
            <p class="alert-danger">Ошибка получения статуса</p>
        <?php endif; ?>
    </td>
</tr>


