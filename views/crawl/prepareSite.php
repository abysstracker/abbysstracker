<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 08.03.2018
 * Time: 1:11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @property $dataProvider
 */

$this->title = 'Подготовка структуры к выгрузке';

?>

<?php $form = ActiveForm::begin([
    'action' => 'export-structure'
]); ?>
<?= Html::submitButton('Экспортировать в файл') ?>

<input name="site_postfix" type="hidden" value="<?=$site_postfix?>">

<?= GridView::widget([
    'dataProvider'=>$dataProvider,
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' =>
                function ($model) {
                    return [
                        'value' => $model['id'],
                    ];
                }
        ],
        'id',
        'url',
        'h1',
        'seo_title',
        'seo_description',
        'seo_keywords',
        ]
]); ?>
<?= Html::submitButton('Экспортировать в файл') ?>
<?php ActiveForm::end() ?>



