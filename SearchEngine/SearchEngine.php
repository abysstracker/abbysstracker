<?php

namespace app\SearchEngine;


abstract class SearchEngine {

    const METHOD = 'GET';
    const REGEXP_ALL_URL = '/<[Aa][\s]{1}[^>]*[Hh][Rr][Ee][Ff][^=]*=[ \'\"\s]*([^ \"\'>\s#]+)[^>]*>/m';
    const REGEX_HREF = '/href="([^"]+)"/';

    public $sQuery;
    public $iCountSite;
    public $iRegionKey;


    protected function getDomainPointerList() {}


    /**
     * @param $sUrl
     * @return string
     */
    public function parseDomain($sUrl) {
        $aUrl = parse_url($sUrl);
        $scheme = !empty($aUrl['scheme']) ? $aUrl['scheme'] : '';
        $sDomain =  $scheme . '://' . $aUrl['host'];

        return $sDomain;
    }

    /**
     * Проверка выдачу на рекламу
     * @param $data
     * @return bool
     */
    protected function isAdvert($data):bool{}

    protected function getConfigHttpClient() {
        $aConfig = [
            'cookies' => true,
            'timeout' => 5.0,
        ];

        return $aConfig;
    }

    protected function compareUrl(){}

    /**
     * Получить контент запроса
     */
    public function getContentOnQuery(){}

    /** Проверка капчи */
    public function checkCaptcha($data) {}

    /** Отправка капчи */
    public function sendCaptcha($data):bool {}

    /**
     * Парсинг страницы на предмет поисковыой выдачи
     * возвращает кусок html кода отвечающего за позицию
     * аналогично поисковой логики
     * @return array|void
     */
    public function getDomainList(){}

    protected function assocFormat($aList = []) {
        $aAssocList = [];

        if (!empty($aList)) {
            foreach($aList as $index => $sDomain) {
                $aAssocList[$index]['domain'] = $sDomain;
            }
        }

        return $aAssocList;
    }

}