<?php

namespace app\SearchEngine;


use app\Components\DomParser;
use app\models\Regions;
use app\models\SearchQuery;
use PHPHtmlParser\Dom;
use yii\base\Exception;
use yii\web\Cookie;

class YandexSearch extends SearchEngine {
    const TEST_FILE_NAME = 'exampleYandex.html';

    /**
     * У яндекса возможны вариации :
     * 10,20,30,40,50
     */
    const ON_PAGE = 50;

    private $urlMask = 'https://yandex.ru/search/?text=[QUERY]&p=[PAGE_NUMBER]&numdoc=[ON_PAGE]&lr=[REGION_KEY]';

    /** @var /GuzzleHttp/Client() */
    private $httpClient = false;

    public $sContent = '';


    /**
     * YandexSearch constructor.
     * @param $iTaskId
     * @throws Exception
     */
    public function __construct($iTaskId) {
        /** @var SearchQuery $oSearchQuery */
        $oSearchQuery = SearchQuery::getOneById($iTaskId);
        $this->setQuery($oSearchQuery->search_text);
        $this->setRegion($oSearchQuery->region_id);
        $this->setHttpClient();
    }


    public function compareUrl($iPage = 0) {
        $sMask = str_replace('[QUERY]', $this->sQuery, $this->urlMask);
        $sMask = str_replace('[PAGE_NUMBER]', $iPage, $sMask);
        $sMask = str_replace('[ON_PAGE]', self::ON_PAGE, $sMask);
        $sUrl = str_replace('[REGION_KEY]', $this->iRegionKey, $sMask);

        return $sUrl;
    }


    public function getContentOnQuery() {

        $content = '';
        if (YII_ENV == 'dev') {
            $content = $this->getTestContent();
            $this->compareUrl(1);
        } else {
            $iCountPage = $this->calcCountPage();

            for ($iPage = 0; $iPage <= $iCountPage; $iPage++) {
                //todo тут точно отдаются правильный юзер агент? Почему compareUrl отдает строку. Это странно
                $oRequest = $this->httpClient
                    ->request(self::METHOD, $this->compareUrl($iPage))->withHeader('User-Agent', \Yii::$app->components['bot_user_agent']);
                $content .= $oRequest->getBody()->getContents();
            }

            $cookieJar = $this->httpClient->getConfig('cookies');
            $cookieJar->toArray();
            $oCookie = \Yii::$app->response->cookies;
            //Установим куки присланные от поисковика
            foreach ($cookieJar->toArray() as $aCookie) {
                $aCookie = array_change_key_case ($aCookie, CASE_LOWER );
                if (isset($aCookie['expires']) OR is_null($aCookie['expires'])) {
                    $aCookie['expire'] = $aCookie['expires'];
                    unset($aCookie['expires']);

                }
                unset($aCookie['max-age']);
                unset($aCookie['discard']);
                unset($aCookie['httponly']); //Не поддерживается yii2

                $oCookie->add(new Cookie($aCookie));
            }

        }

        $this->sContent = $content;
    }

    /** Проверяет что это капча и если да, то вернёт форму */
    public function checkCaptcha($data) {

        $html = new Dom();
        $html->load($this->sContent);
        $aFormCaptcha = $html->find('body form');

        foreach ($aFormCaptcha as $form) {
            if ($form->getAttribute('action') == '/checkcaptcha') {

                //Т.к. нет в парсере функции добавлении ноды, то мы это сделаем вручную
                $formBody = $form->innerHtml;
                $inputAction = '<input type="hidden" name="engine" value="yandex" />';
                $inputAction .= '<input type="hidden" name="captcha" value="yandex" />';
                $csrf = '<input type="hidden" name="_csrf" value="' . \Yii::$app->request->getCsrfToken() . '" />';
                $form = '<form method="post" action="/task/get-domain-list?task_id='. $data['iTaskId']
                    .'">' .$inputAction . $csrf  . $formBody . '</form>';

                return $form;
            }
        }
        return false;
    }

    public function sendCaptcha($aPost):bool {

        unset($aPost['engine']);
        unset($aPost['_csrf']);
        unset($aPost['captcha']);

        $sPost = http_build_query($aPost);
        $captchaGateway = 'https://yandex.ru/checkcaptcha?'. $sPost;

        try {
            $httpClient = new \GuzzleHttp\Client(['cookies' => true, 'timeout' => 5.0, /*'allow_redirects' => false */]);
            $oRequest = $httpClient->request('GET', $captchaGateway)
                ->withHeader('User-Agent', \Yii::$app->components['bot_user_agent']);

            $this->sContent = $oRequest->getBody()->getContents();
        } catch(\Exception $e) {
            //todo Сделать нормальный логгер
            return false;
        }
        return true;
    }


    /**
     * Парсинг страницы на предмет поисковыой выдачи
     * @return array
     */
    public function getDomainList() {
        $aResultHtml = [];

        if (!$this->sContent) {
            return $aResultHtml;
        }

        $aResultHtml = $this->getDomElementLi();
        $this->getDomainResultList($aResultHtml);
        return $aResultHtml;
    }

    /**
     * @TODO снести всё и переписать на ссылкоориентированный подход
     * Достает доменную ссылку из html кода результата поисковой выдачи
     * @param $aResultHtmlList
     */
    public function getDomainResultList(&$aResultHtmlList) {
        $oHtmlParser = DomParser::getParserObject();
        if (!empty($aResultHtmlList)) {

            foreach ($aResultHtmlList as $key => $aDomElement) {
                if (!empty($aDomElement['resultHtml'])) {

                    $sDomElement = $aDomElement['resultHtml'];
                    $oHtmlParser->load($sDomElement);

                    foreach ($oHtmlParser->find('div.organic__path') as $oDomDiv) {
                        //первая ссылка в найденном div - доменная
                        $oDomA = $oDomDiv->find('a', 0);
                        if (!is_null($oDomA) && $oDomA->href) {
                            $aResultHtmlList[$key]['domain'] = $oDomA->href;
                        } else {
                            unset ($aResultHtmlList[$key]);
                        }
                    }
                }
            }
            //очистим хлам
            foreach ($aResultHtmlList as $key => $aDomElement) {
                if (empty($aDomElement['domain'])) {
                    unset($aResultHtmlList[$key]);
                }
            }

        }
    }

    private function setQuery($sQuery) {
        $this->sQuery = urlencode($sQuery);
    }

    /**
     * @throws Exception
     */
    private function setHttpClient() {
        if (!$this->sQuery) {
            throw new Exception("Преждевременная попытка установкеи http клиента, не пределён поисковый запрос");
        }

        $aConfig = $this->getConfigHttpClient();
        $this->httpClient = new \GuzzleHttp\Client($aConfig);

    }

    private function setCountSite($iCountSite) {
        $this->iCountSite = $iCountSite;
    }


    /**
     * @TODO При необходимости организовать логику подсчета старниц здесь
     * Подсчет кол-ва необходимых страниц поисковика
     * минус один, потому что нумерация начинается с нуля
     * @return float
     */
    public function calcCountPage() {
        return 0;
    }

    protected function setRegion($region_id) {
        $iKey = Regions::getKeyOnId($region_id);
        $this->iRegionKey = $iKey;
    }


    /**
     * Тестовый контент с результатами выдачи
     */
    private function getTestContent() {
        return file_get_contents(\Yii::$aliases["@webroot"] . DIRECTORY_SEPARATOR . self::TEST_FILE_NAME);
    }


    /**
     * Прямой парсинг выдачи
     * @return array
     */
    private function getDomElementLi() {
        $aResultHtml = [];
        $oHtmlParser = DomParser::getParserObject();
        $oHtmlParser->load($this->sContent);

        /** @var simple_html_dom $oDomElementUl */
        foreach ($oHtmlParser->find('ul.serp-list') as $oDomElementUl) {
            foreach ($oDomElementUl->find('li') as $key => $li) {
                if (!$this->isAdvert($li->innertext)) {
                    $aResultHtml[$key]['resultHtml'] = $li->innertext;
                } else {
                    $aResultHtml[$key]['resultHtml'] = $this->getAdvertMessage();
                }
            }
        }

        return $aResultHtml;
    }

    /**
     * @todo сделать более корректную проверку на рекламу
     * Проверка рекламной выдачи
     * @param $data
     * @return bool
     */
    protected function isAdvert($data):bool  {
        if (strripos($data, 'реклама</div>') !== false) {
            return true;
        }
        return false;
    }

    private function getAdvertMessage() {
        return '<b><h3>Вероятно, здесь рекламная выдача.</h3></b>';
    }
}
