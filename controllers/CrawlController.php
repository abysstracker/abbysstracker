<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 25.02.2018
 * Time: 20:29
 */

namespace app\controllers;

use app\crawl\Limiter;
use app\crawl\ParseLink;
use app\crawl\ParseSites;
use app\crawl\Status;
use app\models\ParsedSites;
use app\models\Structures;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


/**
 * Паук
 * Class CrawlController
 * @deprecated
 * @package app\controllers
 */
class CrawlController extends Controller {



    function actionIndex() {

        //Ставим задачу на парсинг структуры сайта
        $targetSitesToParse = new ParseSites();
        $targetSitesToParse->start();

        return;

        /**
         * Парсим ссылки
         */
        $oParseStructure = new ParseLink();
        do {//todo Нечего гонять просто так если кончились задачи
            $mRes = $oParseStructure->start();

            if ($mRes == Status::FINISH OR $mRes == Status::ERROR)
                return;

        } while (Limiter::checkTime());
//
//        return $this->render('index');
    }

    /**
     * @deprecated
     */
    public function actionSet() {

//        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
//        $channel = $connection->channel();
//
//        $channel->queue_declare('user0', false, false, false, false);
//
//        $msg = new AMQPMessage('jj World!');
//        $channel->basic_publish($msg, '', 'user0');
//
//
//        echo " [x] Sent 'Hello World!'\n";
//
//        $channel->close();
//        $connection->close();





    }

}