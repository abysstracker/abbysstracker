<?php
namespace app\controllers;

use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Class AuthController
 */
class AuthController extends Controller {

    public $layout = 'auth';
    public $iFineStatus = 10;
    public $iPolicyAdmin = 1009;
    public $sRoleAdmin = 'admin';

    /**
     * Двухэтапная авторизация
     *
     * 1 - авторизация через canapeid
     * ```php
     * return attributes canapeid $eauth->getAttributes()
     * [
     *      'id' => int 0,
     *      'username' => string '',
     *      'email' => string ''
     * ]
     * ```
     * 2 - авторизация Yii (RBAC)
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionLogin() {
        if (Yii::$app->user->isGuest) {

            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity('canapeid');

            // запоминаем url, для редиректа после авторизации
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());

            try {
                /** @var $eauth \common\components\oauth\CanapeIdOAuth2Service */
                if ($eauth->authenticate()) {
                    $sUserEmail = $eauth->getAttribute('email','');
                    $oUser = null;

                    if ($sUserEmail) {
                        $oUser = User::findOne(['email' => $sUserEmail]);

                        if (!$oUser) {

                            $oUser = new User();
                            $oUser->email = $sUserEmail;
                            $oUser->status = $this->iFineStatus;

                            if ($oUser->validate() && $oUser->save()) {
                                $auth = Yii::$app->authManager;
                                $auth->assign($auth->getRole($this->sRoleAdmin), $oUser->id);
                            }
                        }


                        if ($oUser) {
                            Yii::$app->user->login($oUser);
                        }

                        if (!Yii::$app->user->isGuest) {
                            Yii::info('Авторизация пользователя Yii: ' . $sUserEmail, 'auth');
                            return $this->goBack();
                        }

                    }

                    Yii::error('Ошибка авторизации', 'auth');
                    Yii::error($eauth->getAttributes(), 'auth');

                    throw new ForbiddenHttpException('Ошибка авторизации', 403);

                } else {
                    // Если еще не аутентифицированы - переходим на oauth2 форму аутентификации в canape-id.com (redirect to cancelUrl)
                    $eauth->cancel();
                }

            } catch (\ErrorException $e) {
                throw new ForbiddenHttpException($e->getMessage());
            }

        } else {
            return $this->goHome();
        }
    }


    /**
     * Выход пользователя
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->actionIndex();
    }

    /**
     * Страница авторизации
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionIndex() {

        /** @var $eauth \nodge\eauth\ServiceBase */
        $eauth = Yii::$app->get('eauth')->getIdentity('canapeid');

        if (!Yii::$app->user->isGuest && $eauth->authenticate()) {
            return $this->goHome();
        } else {
            return $this->actionLogin();

//            return $this->render('login.twig');
        }
    }


}
