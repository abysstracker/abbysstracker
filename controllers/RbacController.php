<?php

namespace app\controllers;

use frontend\models\User;
use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    private $aNewPolicy = [
        '100' => 'low_manager',
        '201' => 'low_manager',
        '301' => 'manager',
        '401' => 'high_manager',
        '451' => 'director',
        '501' => 'expert',
        '1009' => 'admin',
    ];

    /**
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        /** todo: роли */

        // Создадим роли
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $auth->add($admin);

        // Создадим роли
        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $auth->add($user);

        /** todo: разрешения */

        // Создаем разрешения
        /** todo: вот тут можно разграничить доступ к системе приоритетов парсинга */
        $settings_show_priority = $auth->createPermission('show_priority_system');
        $settings_show_priority->description = 'Доступ к системе приоритетов';
        $auth->add($settings_show_priority);

        $settings_management = $auth->createPermission('management');
        $settings_management->description = 'Доступ к работе с парсером (создание, просмотр, выгрузка, удаление)';
        $auth->add($settings_management);


        /** todo: наследования */

        $auth->addChild($admin, $user);

        /** todo: назначение ролей пользователям */

//        $aUser = User::find()->select(['id','policy'])->where(['between', 'policy', 100,1009])->asArray()->all();
//
//        if ($aUser) {
//            foreach ($aUser as $aData){
//                if (isset( $this->aNewPolicy[$aData['policy']])){
//                    $sNameRole = $this->aNewPolicy[$aData['policy']];
//                    $auth->assign($auth->getRole($sNameRole), $aData['id']);
//                }
//            }
//        }
    }
}