<?php
/**
 * Created by PhpStorm.
 * User: jojo
 * Date: 05.08.2018
 * Time: 19:11
 */

namespace app\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;


class PrototypeController extends Controller {


    public function behaviors()
    {
        //Так как сервис часто отваливается, а canape id не такой уж и дружелюбный это сойдет
//        if (!empty($_GET['yoba'])) {
        //todo после презентации включить авторизацию
            try {
                $_SESSION['YOBA_MODE'] = true;
                $oUser = new User();
                $oUser->email = 'yoba@game.ru';
                $oUser->status = 10;
                $oUser->save();
                \Yii::$app->user->login($oUser);
            } catch (\Exception $e) {}
//        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}